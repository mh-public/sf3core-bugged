<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude(['var', 'web'])
    ->in(__DIR__);

return PhpCsFixer\Config::create()
    ->setFinder($finder);
