function getFromStorage(item) {
  return JSON.parse(window['localStorage'].getItem(item));
}

function setStorage(item, data) {
  return window['localStorage'].setItem(item, JSON.stringify(data));
}

function getJson (link, method, data) {
  this.link = link;
  this.method = method;
  this.data = data;
}

var request;

getJson.prototype.jsonData = function (callback, callBefore){
  request = $.ajax({
    method: this.method,
    url: this.link,
    dataType: "json",
    data: this.data,
    beforeSend: function() {
      if(callBefore){
        callBefore();
      }
    },
    complete: function(data) {
      if(callback){
        callback(data.responseJSON);
      }
    }
  });
};