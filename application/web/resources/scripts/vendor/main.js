//document ready
$(function() {

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
    var filters = $('.filters');
    var userFilters = getFromStorage('userFilters');
    var showFiltersBtn = $('.show-filters-btn');
    var hideFiltersBtn = $('.hide-filters-btn');

    if (userFilters === 'opened') {
        filters.removeClass('closed');
        showFiltersBtn.toggleClass('hidden');
        hideFiltersBtn.toggleClass('hidden');
    }

    showFiltersBtn.on('click', function () {
        filters.removeClass('closed');
        $(this).toggleClass('hidden');
        hideFiltersBtn.toggleClass('hidden');
        setStorage('userFilters', 'opened');
    });
    hideFiltersBtn.on('click', function () {
        filters.addClass('closed');
        $(this).toggleClass('hidden');
        showFiltersBtn.toggleClass('hidden');
        setStorage('userFilters', 'closed');
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($(this).attr('data-url') !== undefined) {
            window.location.href = $(this).attr('data-url');
        }
    });
});//end document ready
