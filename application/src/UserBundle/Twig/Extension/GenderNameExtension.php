<?php

namespace UserBundle\Twig\Extension;

use UserBundle\Entity\User;

class GenderNameExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('genderName', [$this, 'gender']),
        ];
    }

    /**
     * @param int $genderId
     *
     * @return string
     */
    public function gender(int $genderId): string
    {
        $gender = '';
        switch ($genderId) {
            case User::GENDER_MALE:
                $gender = 'male';
                break;
            case User::GENDER_FEMALE:
                $gender = 'female';
                break;
            case User::GENDER_OTHER:
                $gender = 'other';
                break;
        }

        return $gender;
    }
}
