<?php

namespace UserBundle\Handlers;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @DI\Service("user.handlers.role_handler")
 */
class RoleHandler
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $checkerInterface;

    /**
     * @param ContainerInterface            $container
     * @param AuthorizationCheckerInterface $checkerInterface
     */
    public function __construct(ContainerInterface $container, AuthorizationCheckerInterface $checkerInterface)
    {
        $this->container = $container;
        $this->checkerInterface = $checkerInterface;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @return array
     */
    public function getRolesForForm()
    {
        $roles = $this->container->getParameter('security.role_hierarchy.roles');
        if (!$this->checkerInterface->isGranted('ROLE_SUPER_ADMIN')) {
            unset($roles['ROLE_SUPER_ADMIN']);
        }

        if (!$this->checkerInterface->isGranted('ROLE_CONFIGURATORR')) {
            unset($roles['ROLE_CONFIGURATOR']);
        }

        $result = [];
        foreach ($roles as $key => $role) {
            $displayRoleName = ucwords(strtolower(str_replace('_', ' ', $key)));
            $result[$displayRoleName] = $key;
        }

        return $result;
    }
}
