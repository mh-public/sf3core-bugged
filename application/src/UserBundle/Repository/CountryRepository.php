<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class CountryRepository extends EntityRepository
{
    /**
     * @param array $params
     *
     * @return QueryBuilder
     *
     * @SuppressWarnings("PHPMD.UnusedLocalVariable")
     */
    public function getCountriesByFilter(array $params = []): QueryBuilder
    {
        $query = $this->createQueryBuilder('c')
            ->select('c');

        if (isset($params['filters'])) {
            $filters = $params['filters'];
            foreach ($filters as $key => $filter) {
                $methodName = sprintf('filter%s', ucwords($key));
                if (method_exists($this, $methodName)) {
                    call_user_func_array([$this, $methodName], [$query, $filters]);
                }
            }
        }

        if (isset($params['order_by'])) {
            foreach ($params['order_by'] as $key => $direction) {
                $query->addOrderBy(sprintf('c.%s', $key), $direction);
            }
        }

        return $query;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterName(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['name'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('c.name', ':name'))
                ->setParameter('name', $filters['name']);
        }
    }
}
