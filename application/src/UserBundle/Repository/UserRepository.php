<?php

namespace UserBundle\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use UserBundle\Entity\User;

class UserRepository extends EntityRepository
{
    /**
     * @param array|null $params
     *
     * @return array
     */
    public function getUsersByFilterResult(array $params = null): array
    {
        return $this->getUsersByFilter($params)->getQuery()->getArrayResult();
    }

    /**
     * @param array|null $params
     *
     * @return QueryBuilder
     *
     * @SuppressWarnings("PHPMD.UnusedLocalVariable")
     */
    public function getUsersByFilter(array $params = null): QueryBuilder
    {
        $query = $this->createQueryBuilder('u')
            ->select('u')
            ->leftJoin('u.country', 'c');

        if (isset($params['filters'])) {
            $filters = $params['filters'];
            foreach ($filters as $key => $filter) {
                $methodName = sprintf('filter%s', ucwords($key));
                if (method_exists($this, $methodName)) {
                    call_user_func_array([$this, $methodName], [$query, $filters]);
                }
            }
        }

        if (isset($params['order_by'])) {
            foreach ($params['order_by'] as $key => $direction) {
                $query->addOrderBy(sprintf('u.%s', $key), $direction);
            }
        }

        return $query;
    }

    /**
     * @param QueryBuilder $query
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterCountry(QueryBuilder $query, array $filters)
    {
        $country = $filters['country'];
        if (isset($country)) {
            if (is_array($country)) {
                $country = implode(',', $filters['country']);
            }
            $query->andWhere('c.id IN (:country)')
                ->setParameter('country', $country);
        }
    }

    /**
     * @param QueryBuilder $query
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterRole(QueryBuilder $query, array $filters)
    {
        if (isset($filters['role'])) {
            $query->andWhere('u.roles LIKE :role')
                ->setParameter('role', sprintf('%%%s%%', $filters['role']));
        }
    }

    /**
     * @param QueryBuilder $query
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterGender(QueryBuilder $query, array $filters)
    {
        if (isset($filters['gender']) && in_array($filters['gender'], (new User())->getGendersArray())) {
            $query->andWhere('u.gender IN (:gender)')
                ->setParameter('gender', $filters['gender']);
        }
    }

    /**
     * @param QueryBuilder $query
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterSearch(QueryBuilder $query, array $filters)
    {
        if (isset($filters['search'])) {
            $query->andWhere(
                $query->expr()->orX(
                    $query->expr()->like('u.usernameCanonical', ':search'),
                    $query->expr()->like('u.emailCanonical', ':search'),
                    $query->expr()->like('u.firstName', ':search'),
                    $query->expr()->like('u.lastName', ':search'),
                    $query->expr()->like('u.nickname', ':search')
                )
            )->setParameter('search', sprintf('%%%s%%', strtolower($filters['search'])));
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterEmail(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['email'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.email', ':email'))
                ->setParameter('email', $filters['email'], Type::STRING);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterUsername(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['username'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.username', ':username'))
                ->setParameter('username', $filters['username'], Type::STRING);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterNickname(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['nickname'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.nickname', ':nickname'))
                ->setParameter('nickname', $filters['nickname'], Type::STRING);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterFirstName(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['firstName'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.firstName', ':firstName'))
                ->setParameter('firstName', $filters['firstName'], Type::STRING);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterLastName(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['lastName'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.lastName', ':lastName'))
                ->setParameter('lastName', $filters['lastName'], Type::STRING);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array        $filters
     *
     * @SuppressWarnings("unused")
     */
    private function filterEnabled(QueryBuilder $queryBuilder, array $filters)
    {
        if (isset($filters['enabled'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.enabled', ':enabled'))
                ->setParameter('enabled', $filters['enabled'], Type::INTEGER);
        }
    }
}
