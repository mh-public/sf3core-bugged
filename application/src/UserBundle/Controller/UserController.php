<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as SensioConfiguration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;
use UserBundle\Form\Type\UserType;
use UserBundle\Security\UserVoter;
use UserBundle\Service\UserManager;

/**
 * @SensioConfiguration\Security("has_role('ROLE_CONFIGURATOR')")
 */
class UserController extends Controller
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param Request $request
     *
     * @SensioConfiguration\Route("/admin/users", name="users")
     *
     * @return Response
     */
    public function usersAction(Request $request)
    {
        $params = [];
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $this->userManager->getUsersByFilter($params, $this->getUser()),
            $request->query->getInt('page', 1),
            $this->getParameter('per_page'),
            ['wrap-queries' => true]
        );

        return $this->render('UserBundle:User:users.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Request $request
     * @param User    $user
     *
     * @SensioConfiguration\Route("/admin/user/{id}", defaults={"id" = null}, name="users-new")
     *
     * @return Response
     */
    public function newAction(Request $request, User $user = null)
    {
        if ($user) {
            $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);
        }

        if (!$user) {
            $user = new User();
        }

        $form = $this->createForm(UserType::class, $user, ['loggedUser' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userManager->updateUser($user);

            return $this->redirectToRoute('users');
        }

        return $this->render('UserBundle:User:new-user.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @SensioConfiguration\Route("/admin/user/{id}/detail", name="users-detail")
     *
     * @param User $user
     *
     * @return Response
     */
    public function showAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::VIEW, $user);

        return $this->render('UserBundle:User:show-user.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @SensioConfiguration\Route("/admin/user/{id}/delete", name="users_delete")
     *
     * @param User $user
     *
     * @return RedirectResponse
     */
    public function deleteAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::DELETE, $user);
        $this->userManager->deleteUser($user);

        return $this->redirect($this->generateUrl('users'));
    }
}
