<?php

namespace UserBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use UserBundle\Repository\CountryRepository;

class CountryManager implements CountryManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByName($name)
    {
        return $this->getCountryRepository()->findOneBy(['name' => $name]);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountriesByFilter(array $params = []): QueryBuilder
    {
        return $this->getCountryRepository()->getCountriesByFilter($params);
    }

    /**
     * @return CountryRepository|ObjectRepository
     */
    private function getCountryRepository(): CountryRepository
    {
        return $this->entityManager->getRepository('UserBundle:Country');
    }
}
