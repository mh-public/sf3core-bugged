<?php

namespace UserBundle\Service;

use Doctrine\ORM\QueryBuilder;
use UserBundle\Entity\Country;

interface CountryManagerInterface
{
    /**
     * @param string $name
     *
     * @return Country|null|object
     */
    public function findOneByName($name);

    /**
     * @param array $params
     *
     * @return QueryBuilder
     */
    public function getCountriesByFilter(array $params = []): QueryBuilder;
}
