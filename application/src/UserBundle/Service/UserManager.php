<?php

namespace UserBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\QueryBuilder;
use FOS\UserBundle\Doctrine\UserManager as FosUserManager;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

class UserManager extends FosUserManager implements UserManagerInterface
{
    /**
     * @param PasswordUpdaterInterface $passwordUpdater
     * @param CanonicalFieldsUpdater   $canonicalFieldsUpdater
     * @param ObjectManager            $objectManager
     * @param string                   $class
     */
    public function __construct(
        PasswordUpdaterInterface $passwordUpdater,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        ObjectManager $objectManager,
        string $class
    ) {
        parent::__construct($passwordUpdate, $canonicalFieldsUpdater, $objectManager, $class);
    }

    /**
     * {@inheritdoc}
     */
    public function getUsersByFilter(array $params, User $user): QueryBuilder
    {
        return $this->getUserRepository()->getUsersByFilter($params);
    }

    /**
     * @return UserRepository|ObjectRepository
     */
    private function getUserRepository(): UserRepository
    {
        return $this->getRepository();
    }
}
