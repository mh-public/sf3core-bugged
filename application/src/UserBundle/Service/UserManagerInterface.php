<?php

namespace UserBundle\Service;

use Doctrine\ORM\QueryBuilder;
use UserBundle\Entity\User;

interface UserManagerInterface
{
    /**
     * @param array $params
     * @param User  $user
     *
     * @return QueryBuilder
     */
    public function getUsersByFilter(array $params, User $user): QueryBuilder;
}
