<?php

namespace UserBundle\Entity;

use ApiBundle\Entity\AccessToken;
use ApiBundle\Entity\RefreshToken;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class User extends BaseUser
{
    /**
     * @const int GENDER_DEFAULT
     */
    const GENDER_DEFAULT = 0;

    /**
     * @const int GENDER_MALE
     */
    const GENDER_MALE = 1;

    /**
     * @const int GENDER_FEMALE
     */
    const GENDER_FEMALE = 2;

    /**
     * @const int GENDER_OTHER
     */
    const GENDER_OTHER = 3;

    /**
     * @const string ROLE_ADMIN
     */
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @const string ROLE_CONFIGURATOR
     */
    const ROLE_CONFIGURATOR = 'ROLE_CONFIGURATOR';

    /**
     * @const int PER_PAGE
     */
    const PER_PAGE = 20;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"user", "cgetGames", "round", "cgetUsers", "customer", "directMessage", "ranking", "usersRoundRankings", "team", "cgetNewGames"})
     */
    protected $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     *
     * @Assert\DateTime()
     * @Assert\LessThanOrEqual("-18 years")
     * @Assert\GreaterThanOrEqual("-99 years")
     *
     * @Serializer\Groups({"user", "cgetUsers"})
     */
    protected $birthday;

    /**
     * @var int
     *
     * @ORM\Column(name="gender", type="integer", length=20, nullable=false)
     *
     * @Assert\NotNull()
     * @Assert\Type("integer")
     * @Assert\Range(min="0", max="3")
     *
     * @Serializer\Groups({"user", "cgetUsers"})
     */
    protected $gender = 0;

    /**
     * @var \DateTime|null
     *
     * @Serializer\SerializedName("signUpDate")
     * @ORM\Column(name="sign_up_date", type="datetime", nullable=true)
     *
     * @Assert\DateTime()
     *
     * @Serializer\Groups({"user", "cgetUsers"})
     */
    private $signUpDate;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="users")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=true)
     *
     * @Serializer\MaxDepth(1)
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "cgetUsers"})
     */
    private $country;

    /**
     * @var null|string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     *
     * @Serializer\Groups({"user", "cgetUsers"})
     * @Serializer\Expose()
     */
    private $avatar;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     * @Assert\Type(type="string")
     *
     *
     * @Serializer\Groups({"user", "cgetUsers", "cgetNewGames"})
     * @Serializer\Expose()
     */
    private $nickname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", length=50, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     min="2",
     *     max="50"
     * )
     *
     * @Serializer\Groups({"user", "cgetUsers", "cgetNewGames"})
     * @Serializer\Expose()
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", length=50, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     min="2",
     *     max="50"
     * )
     *
     * @Serializer\Groups({"user", "cgetUsers", "cgetNewGames"})
     * @Serializer\Expose()
     */
    private $lastName;

    /**
     * @var ArrayCollection|AccessToken[]
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\AccessToken", mappedBy="user", cascade={"persist", "remove"})
     *
     * @Serializer\Exclude()
     *
     * @SuppressWarnings("unused")
     */
    private $accessTokens;

    /**
     * @var ArrayCollection|RefreshToken[]
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\RefreshToken", mappedBy="user", cascade={"persist", "remove"})
     *
     * @Serializer\Exclude()
     *
     * @SuppressWarnings("unused")
     */
    private $refreshTokens;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return bool
     */
    public function isPasswordLegal(): bool
    {
        if ($this->plainPassword) {
            return $this->username !== $this->plainPassword;
        }

        return true;
    }

    /**
     * @param Country $country
     *
     * @return User
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return null|\DateTime
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime|null $birthday
     *
     * @return User
     */
    public function setBirthday(\DateTime $birthday = null): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     *
     * @return User
     */
    public function setGender($gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getSignUpDate(): ?\DateTime
    {
        return $this->signUpDate;
    }

    /**
     * @param \DateTime $signUpDate
     *
     * @return User
     */
    public function setSignUpDate($signUpDate): self
    {
        $this->signUpDate = $signUpDate;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @throws \Exception
     *
     * @codeCoverageIgnore
     */
    public function prePersist()
    {
        $this->setSignUpDate(new \DateTime())
            ->setUsername($this->getEmailCanonical());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setUsername($this->getEmailCanonical());
    }

    /**
     * @return array
     */
    public function getGendersArray(): array
    {
        return [
            self::GENDER_DEFAULT => self::GENDER_DEFAULT,
            self::GENDER_MALE => self::GENDER_MALE,
            self::GENDER_FEMALE => self::GENDER_FEMALE,
            self::GENDER_OTHER => self::GENDER_OTHER,
        ];
    }

    /**
     * @return null|string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param null|string $avatar
     *
     * @return self
     */
    public function setAvatar(?string $avatar): self
    {
        if (null !== $avatar && '' !== $avatar) {
            $this->avatar = $avatar;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isConfigurator(): bool
    {
        return $this->hasRole(static::ROLE_CONFIGURATOR);
    }

    /**
     * @return null|string
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param null|string $nickname
     *
     * @return self
     */
    public function setNickname(?string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param null|string $firstName
     *
     * @return self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     *
     * @return self
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
