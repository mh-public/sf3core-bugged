<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="countries")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\CountryRepository")
 *
 * @UniqueEntity(fields={"name", "shortName"})
 *
 * @Serializer\ExclusionPolicy("NONE")
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Country
{
    /**
     * @const int PER_PAGE
     */
    const PER_PAGE = 20;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"country", "cgetUsers", "user"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     *
     * @Assert\Range(min="4", max="64")
     */
    private $name;

    /**
     * @var string
     *
     * @JMS\SerializedName("shortName")
     * @ORM\Column(name="short_name", type="string", length=10, unique=true)
     *
     * @Assert\Range(min="2", max="64")
     */
    private $shortName;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     * @ORM\OneToMany(targetEntity="User", mappedBy="country")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     *
     * @codeCoverageIgnore
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     *
     * @return self
     */
    public function setShortName($shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function addUser(User $user): self
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
