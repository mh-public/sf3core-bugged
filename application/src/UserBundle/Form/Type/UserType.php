<?php

namespace UserBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as FieldType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\Country;
use UserBundle\Entity\User;
use UserBundle\Form\DataTransformer\RoleTransformer;
use UserBundle\Handlers\RoleHandler;
use UserBundle\Service\CustomerManagerInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UserType extends AbstractType
{
    /**
     * @var RoleHandler
     */
    private $roleHandler;

    /**
     * @var CustomerManagerInterface
     */
    private $customerManager;

    /**
     * @param RoleHandler              $roleHandler
     * @param CustomerManagerInterface $customerManager
     */
    public function __construct(RoleHandler $roleHandler, CustomerManagerInterface $customerManager)
    {
        $this->roleHandler = $roleHandler;
        $this->customerManager = $customerManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $loggedUser */
        $loggedUser = $options['loggedUser'];
        $builder
            ->add('nickname', FieldType\TextType::class, [
                'required' => false,
            ])
            ->add('email', FieldType\EmailType::class)
            ->add('enabled', FieldType\CheckboxType::class)
            ->add('gender', FieldType\ChoiceType::class, [
                'choices' => [
                    'male' => User::GENDER_MALE,
                    'female' => User::GENDER_FEMALE,
                    'other' => User::GENDER_OTHER,
                ],
                'placeholder' => 'Choose...',
                'required' => true,
            ])
            ->add('birthday', FieldType\DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => false,
            ])
            ->add('roles', FieldType\ChoiceType::class, [
                'choices' => $this->roleHandler->getRolesForForm(),
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose...',
                'required' => false,
            ])
            ->add('plainPassword', FieldType\RepeatedType::class, [
                'type' => FieldType\PasswordType::class,
                'required' => false,
                'first_options' => ['label' => 'form.password', 'translation_domain' => 'FOSUserBundle'],
                'second_options' => ['label' => 'form.password_confirmation', 'translation_domain' => 'FOSUserBundle'],
            ])
            ->add('avatar', FieldType\TextareaType::class, [
                'required' => false,
            ])
            ->add('firstName', FieldType\TextType::class, [
                'required' => false,
            ])
            ->add('lastName', FieldType\TextType::class, [
                'required' => false,
            ]);

        $builder->get('roles')
            ->addModelTransformer(new RoleTransformer($this->roleHandler));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'attr' => ['novalidate' => 'novalidate'],
            'csrf_protection' => true,
            'allow_extra_fields' => true,
            'validation_groups' => function (FormInterface $form) {
                $validationGroups = ['Default'];
                $data = $form->getData();
                if (empty($data->getId()) || (!empty($data->getId()) && !empty($data->getPlainPassword()))) {
                    array_push($validationGroups, 'createUser');
                }

                return $validationGroups;
            },
        ])
            ->setRequired(['loggedUser']);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'user_bundle_user_type';
    }
}
