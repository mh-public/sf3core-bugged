<?php

namespace UserBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use UserBundle\Handlers\RoleHandler;

class RoleTransformer implements DataTransformerInterface
{
    /**
     * @var RoleHandler
     */
    protected $roleHandler;

    /**
     * @param RoleHandler $roleHandler
     */
    public function __construct(RoleHandler $roleHandler)
    {
        $this->roleHandler = $roleHandler;
    }

    /**
     * @param array|null $roles
     *
     * @return string
     */
    public function transform($roles)
    {
        if (null === $roles) {
            return '';
        }

        return reset($roles);
    }

    /**
     * @param string $role
     *
     * @return array|null
     *
     * @throws TransformationFailedException if object (issue) is not found
     */
    public function reverseTransform($role)
    {
        if (!$role) {
            return [];
        }

        $roles = $this->roleHandler->getRolesForForm();

        if (!in_array($role, $roles)) {
            throw new TransformationFailedException(sprintf(
                'Role %s does not exist!',
                $role
            ));
        }

        return [$role];
    }
}
