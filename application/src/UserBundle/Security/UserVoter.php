<?php

namespace UserBundle\Security;

use CoreBundle\Security\AbstractVoter;
use UserBundle\Entity\User;

class UserVoter extends AbstractVoter
{
    /**
     * @const array ROLES_WITH_FULL_ACCESS
     */
    protected const ROLES_WITH_FULL_ACCESS = ['ROLE_SUPER_ADMIN'];

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::VIEW, self::EDIT, self::DELETE]) && $subject instanceof User;
    }

    /**
     * {@inheritdoc}
     */
    protected function canView($subject, User $currentUser): bool
    {
        /* @var User $subject */
        return $currentUser === $subject || $subject->getCustomer() === $currentUser->getCustomer();
    }

    /**
     * {@inheritdoc}
     */
    protected function canEdit($subject, User $currentUser): bool
    {
        /** @var User $subject */
        if ($subject->isSuperAdmin() && !$currentUser->isSuperAdmin()) {
            return false;
        }

        if ($currentUser->isConfigurator()) {
            return true;
        }

        return $currentUser === $subject;
    }

    /**
     * {@inheritdoc}
     */
    protected function canDelete($subject, User $curentUser): bool
    {
        return $this->canEdit($subject, $curentUser);
    }
}
