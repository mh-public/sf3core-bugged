<?php

namespace UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\Country;
use UserBundle\Entity\User;

/**
 * @codeCoverageIgnore
 */
class LoadUserData extends Fixture
{
    /**
     * @param ObjectManager $entityManager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $entityManager)
    {
        /** @var Country $country */
        $country = $entityManager->getRepository('UserBundle:Country')->findOneBy(['name' => 'Poland']);

        $user = new User();
        $user->setUsername('test3');
        $user->setEmail('test3@test3.pl');
        $user->setBirthday(new \DateTime('1988-01-01'));
        $user->setGender(1);
        $user->setRoles(['ROLE_USER']);
        $user->setEnabled(true);
        $user->setPlainPassword('Admin1234');
        $user->setCountry($country);

        $entityManager->persist($user);
        $entityManager->flush();
    }
}
