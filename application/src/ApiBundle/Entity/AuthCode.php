<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\OAuthServerBundle\Entity\AuthCode as BaseAuthCode;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="auth_codes")
 * @ORM\Entity
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class AuthCode extends BaseAuthCode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User")
     *
     * @Assert\NotBlank()
     */
    protected $user;
}
