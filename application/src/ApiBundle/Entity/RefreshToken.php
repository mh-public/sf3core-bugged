<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\OAuthServerBundle\Entity\RefreshToken as BaseRefreshToken;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="refresh_tokens")
 * @ORM\Entity
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class RefreshToken extends BaseRefreshToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotBlank()
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="refreshTokens")
     *
     * @Assert\NotBlank()
     */
    protected $user;
}
