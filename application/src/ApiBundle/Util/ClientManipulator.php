<?php

namespace ApiBundle\Util;

use FOS\OAuthServerBundle\Model\ClientInterface;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;

class ClientManipulator
{
    /**
     * @var ClientManagerInterface
     */
    protected $clientManager;

    /**
     * @param ClientManagerInterface $clientManager
     */
    public function __construct(ClientManagerInterface $clientManager)
    {
        $this->clientManager = $clientManager;
    }

    /**
     * @param array $redirectUri
     * @param array $grantTypes
     *
     * @return ClientInterface
     */
    public function create($redirectUri, $grantTypes)
    {
        $client = $this->clientManager->createClient();
        $client->setRedirectUris($redirectUri);
        $client->setAllowedGrantTypes($grantTypes);

        $this->clientManager->updateClient($client);

        return $client;
    }
}
