<?php

namespace ApiBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelInterface;

class ApiExceptionListener
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @throws \ReflectionException
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ('prod' === $this->kernel->getEnvironment() && false !== strpos($event->getRequest()->get('_route'), 'api_')) {
            $exception = $event->getException();

            if ($this->isNotExcluded((new \ReflectionClass($exception))->getShortName())) {
                $code = Response:HTTP_BAD_REQUEST;
                if (!empty($exception->getCode()) || 0 !== $exception->getCode()) {
                    $code = $exception->getCode();
                }

                $responseData = [
                    'error' => [
                        'code' => $code,
                        'message' => $exception->getMessage(),
                    ],
                ];

                $event->setResponse(new JsonResponse($responseData, $code));
            }
        }
    }

    /**
     * @param string $className
     *
     * @return bool
     */
    private function isNotExcluded(string $className): bool
    {
        $excludedExceptions = ['Doctrine'];
        foreach ($excludedExceptions as $excludedException) {
            if (false !== strpos($className, $excludedException)) {
                return false;
            }
        }

        return true;
    }
}
