<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as FOSAnnotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View as FOSView;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as SensioConfiguration;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\Country;
use UserBundle\Service\CountryManagerInterface;

/**
 * @FOSAnnotations\RouteResource("Country")
 * @FOSAnnotations\NamePrefix("api_")
 * @FOSAnnotations\View(serializerEnableMaxDepthChecks=true)
 */
class CountryController extends FOSRestController
{
    /**
     * @var CountryManagerInterface
     */
    private $countryManager;

    /**
     * @param CountryManagerInterface $countryManager
     */
    public function __construct(CountryManagerInterface $countryManager)
    {
        $this->countryManager = $countryManager;
    }

    /**
     * Get specific country by country ID.
     *
     * @SWG\Tag(
     *     name="Countries",
     *     description="Country API section"
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Returns country",
     *     @SWG\Schema(
     *         @SWG\Property(property="data", type="array", @SWG\Items(ref=@Nelmio\Model(type=Country::class))),
     *     )
     * )
     * @SWG\Parameter(
     *     name="country",
     *     in="path",
     *     description="Country ID",
     *     required=true,
     *     type="integer",
     *     allowEmptyValue=false
     * )
     *
     * @SensioConfiguration\Security("has_role('ROLE_USER')")
     *
     * @param Country $country
     *
     * @return FOSView
     */
    public function getAction(Country $country)
    {
        return new FOSView(['data' => $country], Response::HTTP_OK);
    }

    /**
     * Get paginated countries list with optional filters.
     *
     * @SWG\Tag(
     *     name="Countries",
     *     description="Country API section"
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Returns countries",
     *      @SWG\Schema(
     *         @SWG\Property(property="data",
     *              @SWG\Property(property="current_page_number", type="integer"),
     *              @SWG\Property(property="num_items_per_page", type="integer"),
     *              @SWG\Property(property="items", type="array", @SWG\Items(ref=@Nelmio\Model(type=Country::class))),
     *              @SWG\Property(property="total_count", type="integer"),
     *         ),
     *     )
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset from which to start listing countries",
     *     default="1",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="How many countries to return",
     *     default="20",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="order_by",
     *     in="query",
     *     type="string",
     *     description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filter by fields. Must be an array ie. &filters[name]=Poland",
     *     required=false
     * )
     *
     * @FOSAnnotations\QueryParam(name="offset", requirements="\d+", default="1")
     * @FOSAnnotations\QueryParam(name="limit", requirements="\d+", default="999999")
     * @FOSAnnotations\QueryParam(name="order_by", nullable=true, map=true)
     * @FOSAnnotations\QueryParam(name="filters", nullable=true, map=true)
     *
     * @SensioConfiguration\Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return FOSView
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $params = $paramFetcher->all();
        $pagination = $this->get('knp_paginator')->paginate(
            $this->countryManager->getCountriesByFilter($params),
            $params['offset'],
            $params['limit'],
            ['wrap-queries' => true]
        );

        return new FOSView(['data' => $pagination], Response::HTTP_OK);
    }
}
