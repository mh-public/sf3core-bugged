<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as FOSAnnotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View as FOSView;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as SensioConfiguration;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;
use UserBundle\Form\Type\UserType;
use UserBundle\Security\UserVoter;
use UserBundle\Service\UserManager;

/**
 * @FOSAnnotations\RouteResource("User")
 * @FOSAnnotations\NamePrefix("api_")
 * @FOSAnnotations\View(serializerEnableMaxDepthChecks=true)
 */
class UserController extends FOSRestController
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Get certain user by ID.
     *
     * @SWG\Tag(
     *     name="Users",
     *     description="User API section"
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Returns user",
     *     @SWG\Schema(
     *          @SWG\Property(property="data", type="array", @SWG\Items(ref=@Nelmio\Model(type=User::class))),
     *     )
     * )
     * @SWG\Parameter(
     *     name="user",
     *     in="path",
     *     description="User ID",
     *     required=true,
     *     type="integer",
     *     allowEmptyValue=false
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"Default", "user"})
     *
     * @SensioConfiguration\Security("has_role('ROLE_USER')")
     *
     * @param User $user
     *
     * @return FOSView
     */
    public function getAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::VIEW, $user);

        return new FOSView(['data' => $user], Response::HTTP_OK);
    }

    /**
     * Get paginated users list with optional filters.
     *
     * @SWG\Tag(
     *     name="Users",
     *     description="User API section"
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Returns users",
     *     @SWG\Schema(
     *         @SWG\Property(property="data",
     *              @SWG\Property(property="current_page_number", type="integer"),
     *              @SWG\Property(property="num_items_per_page", type="integer"),
     *              @SWG\Property(property="items", type="array", @SWG\Items(ref=@Nelmio\Model(type=User::class))),
     *              @SWG\Property(property="total_count", type="integer"),
     *         ),
     *     )
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset from which to start listing users",
     *     default="1",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="How many users to return",
     *     default="20",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="order_by",
     *     in="query",
     *     type="string",
     *     description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filter by fields. Must be an array ie. &filters[id]=3",
     *     required=false
     * )
     *
     * @FOSAnnotations\QueryParam(name="offset", requirements="\d+", default="1")
     * @FOSAnnotations\QueryParam(name="limit", requirements="\d+", default="999999")
     * @FOSAnnotations\QueryParam(name="order_by", nullable=true, map=true)
     * @FOSAnnotations\QueryParam(name="filters", nullable=true, map=true)
     * @FOSAnnotations\View(serializerGroups={"Default", "cgetUsers"})
     *
     * @SensioConfiguration\Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return FOSView
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        $params = $paramFetcher->all();
        $pagination = $this->get('knp_paginator')->paginate(
            $this->userManager->getUsersByFilter($params, $this->getUser()),
            $params['offset'],
            $params['limit'],
            ['wrap-queries' => true]
        );

        return new FOSView(['data' => $pagination], Response::HTTP_OK);
    }

    /**
     * Create new user.
     *
     * @SWG\Tag(
     *     name="Users",
     *     description="User API section",
     * )
     * @SWG\Response(
     *     response="201",
     *     description="Create user",
     *     @SWG\Schema(
     *         @SWG\Property(property="data", type="array", @SWG\Items(ref=@Nelmio\Model(type=User::class))),
     *     )
     * )
     * @SWG\Response(
     *     response="400",
     *     description="Invalid input",
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="User form",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="user_bundle_user_type", type="object",
     *             @SWG\Property(property="nickname", type="string"),
     *             @SWG\Property(property="firstName", type="string"),
     *             @SWG\Property(property="lastName", type="string"),
     *             @SWG\Property(property="email", type="string"),
     *             @SWG\Property(property="enabled", type="integer", description="true - enabled, false - not enabled"),
     *             @SWG\Property(property="gender", type="integer", description="0 - default, 1 - male, 2 - female, 3 - other"),
     *             @SWG\Property(property="birthday", type="object", description="1900-01-01"),
     *             @SWG\Property(property="roles", type="string", description="ROLE_USER"),
     *             @SWG\Property(property="country", type="integer", description="See /api/countries"),
     *             @SWG\Property(property="avatar", type="string", description="Avatar file in base64"),
     *             @SWG\Property(property="plainPassword", type="object",
     *                 @SWG\Property(property="first", type="string", description="Password"),
     *                 @SWG\Property(property="second", type="string", description="Confirm password"),
     *             ),
     *         ),
     *     )
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"Default", "user"})
     *
     * @SensioConfiguration\Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param Request $request
     *
     * @return FOSView
     */
    public function postAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, $this->getFormOptions($request));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userManager->updateUser($user, true);

            return new FOSView(['data' => $user], Response::HTTP_CREATED);
        }

        return new FOSView($form, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Update existing user by ID.
     *
     * @SWG\Tag(
     *     name="Users",
     *     description="User API section",
     * )
     * @SWG\Response(
     *     response="200",
     *     description="Update user",
     *     @SWG\Schema(
     *         @SWG\Property(property="data", type="array", @SWG\Items(ref=@Nelmio\Model(type=User::class))),
     *     )
     * )
     * @SWG\Response(
     *     response="400",
     *     description="Invalid input",
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     description="User form",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="user_bundle_user_type", type="object",
     *             @SWG\Property(property="nickname", type="string"),
     *             @SWG\Property(property="firstName", type="string"),
     *             @SWG\Property(property="lastName", type="string"),
     *             @SWG\Property(property="email", type="string"),
     *             @SWG\Property(property="enabled", type="integer", description="true - enabled, false - not enabled"),
     *             @SWG\Property(property="gender", type="integer", description="0 - default, 1 - male, 2 - female, 3 - other"),
     *             @SWG\Property(property="birthday", type="object", description="1900-01-01"),
     *             @SWG\Property(property="roles", type="string", description="ROLE_USER"),
     *             @SWG\Property(property="country", type="integer", description="See /api/countries"),
     *             @SWG\Property(property="avatar", type="string", description="Avatar file in base64"),
     *             @SWG\Property(property="plainPassword", type="object",
     *                 @SWG\Property(property="first", type="string", description="Password"),
     *                 @SWG\Property(property="second", type="string", description="Confirm password"),
     *             ),
     *         ),
     *     )
     * )
     * @SWG\Parameter(
     *     name="user",
     *     in="path",
     *     description="User ID",
     *     required=true,
     *     type="integer",
     *     allowEmptyValue=false
     * )
     *
     * @FOSAnnotations\View(serializerGroups={"Default", "user"})
     *
     * @SensioConfiguration\Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return FOSView
     */
    public function putAction(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::EDIT, $user);

        $request->setMethod('PATCH');
        $form = $this->createForm(UserType::class, $user, $this->getFormOptions($request));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userManager->updateUser($user, true);

            return new FOSView(['data' => $user], Response::HTTP_OK);
        }

        return new FOSView($form, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete existing user by ID.
     *
     * @SWG\Tag(
     *     name="Users",
     *     description="User API section",
     * )
     * @SWG\Response(
     *     response="204",
     *     description="User deleted",
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Content not found",
     * )
     * @SWG\Parameter(
     *     name="user",
     *     in="path",
     *     description="User ID",
     *     required=true,
     *     type="integer",
     *     allowEmptyValue=false
     * )
     *
     * @SensioConfiguration\Security("has_role('ROLE_CONFIGURATOR')")
     *
     * @param User $user
     *
     * @return FOSView
     */
    public function deleteAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::DELETE, $user);
        $this->userManager->deleteUser($user);

        return new FOSView(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function getFormOptions(Request $request): array
    {
        return ['method' => $request->getMethod(), 'csrf_protection' => false, 'loggedUser' => $this->getUser()];
    }
}
