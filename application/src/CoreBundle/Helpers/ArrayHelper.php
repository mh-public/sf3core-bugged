<?php

namespace CoreBundle\Helpers;

class ArrayHelper implements ArrayHelperInterface
{
    /**
     * {@inheritdoc}
     */
    public function changeArrayKeys(array $array, array $keyMap): array
    {
        foreach ($keyMap as $oldKey => $newKey) {
            if (!array_key_exists($oldKey, $array)) {
                continue;
            }

            $keys = array_keys($array);
            $keys[array_search($oldKey, $keys)] = $newKey;
            $array = array_combine($keys, $array);
        }

        return $array;
    }
}
