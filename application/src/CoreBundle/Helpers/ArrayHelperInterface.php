<?php

namespace CoreBundle\Helpers;

interface ArrayHelperInterface
{
    /**
     * @param array $array
     * @param array $keyMap
     *
     * @return array
     */
    public function changeArrayKeys(array $array, array $keyMap): array;
}
