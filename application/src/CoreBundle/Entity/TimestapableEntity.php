<?php

namespace CoreBundle\Entity;

use CoreBundle\EventListener\Entity\TimestampableInterface;
use CoreBundle\EventListener\Entity\UserstampableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
abstract class TimestapableEntity implements TimestampableInterface, UserstampableInterface
{
    /**
     * @var \DateTime
     *
     * @JMS\SerializedName("dateCreated")
     * @ORM\Column(type="datetime")
     *
     * @Assert\DateTime()
     */
    protected $dateCreated;

    /**
     * @var \DateTime
     *
     * @JMS\SerializedName("dateModified")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateModified;

    /**
     * @var User
     *
     * @JMS\MaxDepth(2)
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     *
     * @Assert\NotBlank()
     */
    protected $createdBy;

    /**
     * @var User
     *
     * @JMS\MaxDepth(2)
     * @Gedmo\Blameable(on="update")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="modified_by", referencedColumnName="id", nullable=true)
     */
    protected $modifiedBy;

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     *
     * @return self
     */
    public function setDateCreated(\DateTime $dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     *
     * @return self
     */
    public function setDateModified(\DateTime $dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     *
     * @return self
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return User
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * @param User $modifiedBy
     *
     * @return self
     */
    public function setModifiedBy(User $modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }
}
