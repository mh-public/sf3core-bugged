<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as SensioConfiguration;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SensioConfiguration\Security("has_role('ROLE_USER')")
 */
class DashboardController extends Controller
{
    /**
     * @SensioConfiguration\Route("/admin/dashboard", name="dashboard")
     * @SensioConfiguration\Method("GET")
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('CoreBundle:Dashboard:index.html.twig');
    }
}
