<?php

namespace CoreBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use UserBundle\Entity\User;

abstract class AbstractVoter extends Voter
{
    /**
     * @const string VIEW
     */
    const VIEW = 'view';

    /**
     * @const string VIEW
     */
    const EDIT = 'edit';

    /**
     * @const string DELETE
     */
    const DELETE = 'delete';

    /**
     * @const array ROLES_WITH_FULL_ACCESS
     */
    protected const ROLES_WITH_FULL_ACCESS = ['ROLE_SUPER_ADMIN', 'ROLE_CONFIGURATOR'];

    /**
     * @var AccessDecisionManagerInterface
     */
    protected $decisionManager;

    /**
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * {@inheritdoc}
     */
    public function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $currentUser = $token->getUser();

        if (!$currentUser instanceof User) {
            return false;
        }

        foreach ($this::ROLES_WITH_FULL_ACCESS as $roleWithAccess) {
            if ($this->decisionManager->decide($token, [$roleWithAccess])) {
                return true;
            }
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $currentUser);
            case self::EDIT:
                return $this->canEdit($subject, $currentUser);
            case self::DELETE:
                return $this->canDelete($subject, $currentUser);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param mixed $subject
     * @param User  $currentUser
     *
     * @return bool
     */
    abstract protected function canView($subject, User $currentUser): bool;

    /**
     * @param mixed $subject
     * @param User  $currentUser
     *
     * @return bool
     */
    abstract protected function canEdit($subject, User $currentUser): bool;

    /**
     * @param mixed $subject
     * @param User  $currentUser
     *
     * @return bool
     */
    abstract protected function canDelete($subject, User $currentUser): bool;
}
