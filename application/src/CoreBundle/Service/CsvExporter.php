<?php

namespace CoreBundle\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class CsvExporter implements CsvExporterInterface
{
    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var string
     */
    protected $data;

    /**
     * @var string
     */
    protected $filename = 'data_export';

    /**
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     *
     * @return CsvExporterInterface
     */
    public function setFilename(string $filename): CsvExporterInterface
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function replaceHeaders(array $headers)
    {
        $this->data = str_replace(array_keys($headers), array_values($headers), $this->data);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setData(array $data): CsvExporterInterface
    {
        $this->data = $this->serialize($data);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render(): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Cache-Control', 'public');
        $response->headers->set('Content-Disposition', sprintf('attachment; filename="%s.csv"', $this->getFilename()));
        $response->setContent($this->data);

        return $response;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function serialize(array $data): string
    {
        $normalizedData = $this->serializer->normalize($data);

        return (string) $this->serializer->encode($normalizedData, 'csv', ['csv_delimiter' => ';']);
    }
}
