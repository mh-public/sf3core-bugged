<?php

namespace CoreBundle\Service;

use Symfony\Component\HttpFoundation\Response;

interface CsvExporterInterface
{
    /**
     * @param array $headers
     *
     * @return self
     */
    public function replaceHeaders(array $headers);

    /**
     * @return string
     */
    public function getFilename(): string;

    /**
     * @param string $filename
     *
     * @return self
     */
    public function setFilename(string $filename): CsvExporterInterface;

    /**
     * @param array $data
     *
     * @return self
     */
    public function setData(array $data): CsvExporterInterface;

    /**
     * @return Response
     */
    public function render(): Response;
}
