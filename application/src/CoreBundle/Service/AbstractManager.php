<?php

namespace CoreBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param bool $doFlush
     */
    public function doFlush(bool $doFlush)
    {
        if ($doFlush) {
            $this->entityManager->flush();
            $this->entityManager->clear();
        }
    }

    /**
     * @return mixed
     */
    abstract protected function getRepository();
}
