<?php

namespace CoreBundle\EventListener\Entity;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use UserBundle\Entity\User;

class UserstampableSubscriber implements EventSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof UserstampableInterface) {
            $entity->setCreatedBy($this->getLoggedUser());
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof UserstampableInterface) {
            $entity->setModifiedBy($this->getLoggedUser());
        }
    }

    /**
     * @return User
     */
    protected function getLoggedUser(): User
    {
        if ($this->tokenStorage->getToken() instanceof TokenInterface) {
            return $this->tokenStorage->getToken()->getUser();
        }

        throw new AuthenticationException('Logged user not found.');
    }
}
