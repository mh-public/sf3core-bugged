<?php

namespace CoreBundle\EventListener\Entity;

interface TimestampableInterface
{
    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime;

    /**
     * @param \DateTime $dateCreated
     *
     * @return $this
     */
    public function setDateCreated(\DateTime $dateCreated);

    /**
     * @return \DateTime
     */
    public function getDateModified();

    /**
     * @param \DateTime $dateModified
     *
     * @return $this
     */
    public function setDateModified(\DateTime $dateModified);
}
