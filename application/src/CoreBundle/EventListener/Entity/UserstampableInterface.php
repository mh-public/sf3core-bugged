<?php

namespace CoreBundle\EventListener\Entity;

use UserBundle\Entity\User;

interface UserstampableInterface
{
    /**
     * @param User $createdBy
     *
     * @return $this
     */
    public function setCreatedBy(User $createdBy);

    /**
     * @return User
     */
    public function getCreatedBy(): User;

    /**
     * Set modifiedBy.
     *
     * @param User $modifiedBy
     *
     * @return $this
     */
    public function setModifiedBy(User $modifiedBy);

    /**
     * @return User
     */
    public function getModifiedBy();
}
