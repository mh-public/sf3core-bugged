<?php

namespace CoreBundle\EventListener;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Twig\Error\Error;
use UserBundle\Entity\User;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SecurityListener implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface, LogoutSuccessHandlerInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var TokenStorage
     */
    private $token;

    /**
     * @var CsrfTokenManager
     */
    private $csrfTokenManager;

    /**
     * @param UrlGeneratorInterface $router
     * @param Session               $session
     * @param TwigEngine            $templating
     * @param TokenStorage          $token
     * @param CsrfTokenManager      $csrfTokenManager
     */
    public function __construct(
        UrlGeneratorInterface $router,
        Session $session,
        TwigEngine $templating,
        TokenStorage $token,
        CsrfTokenManager $csrfTokenManager
    ) {
        $this->router = $router;
        $this->session = $session;
        $this->templating = $templating;
        $this->token = $token;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @return JsonResponse|RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $url = $this->router->generate('dashboard');

        $roles = $this->getLoggedUser()->getRoles();
        if (in_array(User::ROLE_CONFIGURATOR, $roles) || in_array(User::ROLE_SUPER_ADMIN, $roles)) {
            $url = $this->router->generate('dashboard');
        }

        if ($this->session->get('_security.main.target_path')) {
            $url = $this->session->get('_security.main.target_path');
        }

        return new RedirectResponse($url);
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @return JsonResponse|Response
     *
     * @throws Error
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $lastUsernameKey = Security::LAST_USERNAME;
        $lastUsername = (null === $this->session) ? '' : $this->session->get($lastUsernameKey);

        $params = [
            'error' => $exception,
            'csrf_token' => $this->csrfTokenManager->getToken('authenticate')->getValue(),
            'last_username' => $lastUsername,
        ];

        return $this->templating->renderResponse('FOSUserBundle:Security:login.html.twig', $params);
    }

    /**
     * @param Request $request
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request): RedirectResponse
    {
        return new RedirectResponse($this->router->generate('fos_user_security_login'));
    }

    /**
     * @return User
     */
    protected function getLoggedUser(): User
    {
        if ($this->token->getToken() instanceof UsernamePasswordToken) {
            return $this->token->getToken()->getUser();
        }

        throw new AuthenticationException('Logged user not found.');
    }
}
