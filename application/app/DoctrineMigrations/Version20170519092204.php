<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170519092204 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS access_tokens (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_58D184BC5F37A13B (token), INDEX IDX_58D184BC19EB6921 (client_id), INDEX IDX_58D184BCA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS auth_codes (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, redirect_uri LONGTEXT NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_298F90385F37A13B (token), INDEX IDX_298F903819EB6921 (client_id), INDEX IDX_298F9038A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS clients (id INT AUTO_INCREMENT NOT NULL, random_id VARCHAR(255) NOT NULL, redirect_uris LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', secret VARCHAR(255) NOT NULL, allowed_grant_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS refresh_tokens (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_9BACE7E15F37A13B (token), INDEX IDX_9BACE7E119EB6921 (client_id), INDEX IDX_9BACE7E1A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS countries (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, short_name VARCHAR(10) NOT NULL, UNIQUE INDEX UNIQ_5D66EBAD5E237E06 (name), UNIQUE INDEX UNIQ_5D66EBAD3EE4B093 (short_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS users (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', birthday DATE DEFAULT NULL, gender INT NOT NULL, sign_up_date DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_1483A5E9A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_1483A5E9C05FB297 (confirmation_token), INDEX IDX_1483A5E9F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE access_tokens ADD CONSTRAINT FK_58D184BC19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE access_tokens ADD CONSTRAINT FK_58D184BCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE auth_codes ADD CONSTRAINT FK_298F903819EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE auth_codes ADD CONSTRAINT FK_298F9038A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE refresh_tokens ADD CONSTRAINT FK_9BACE7E119EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE refresh_tokens ADD CONSTRAINT FK_9BACE7E1A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9F92F3E70 FOREIGN KEY (country_id) REFERENCES countries (id)');
        $this->addSql("INSERT INTO `countries` (`id`, `name`, `short_name`)
            VALUES
                (1, 'Afghanistan', 'AF'),
                (2, 'Albania', 'AL'),
                (3, 'Algeria', 'DZ'),
                (4, 'American Samoa', 'AS'),
                (5, 'Andorra', 'AD'),
                (6, 'Angola', 'AO'),
                (7, 'Anguilla', 'AI'),
                (8, 'Antarctica', 'AQ'),
                (9, 'Antigua and Barbuda', 'AG'),
                (10, 'Argentina', 'AR'),
                (11, 'Armenia', 'AM'),
                (12, 'Aruba', 'AW'),
                (13, 'Australia', 'AU'),
                (14, 'Austria', 'AT'),
                (15, 'Azerbaijan', 'AZ'),
                (16, 'Bahamas', 'BS'),
                (17, 'Bahrain', 'BH'),
                (18, 'Bangladesh', 'BD'),
                (19, 'Barbados', 'BB'),
                (20, 'Belarus', 'BY'),
                (21, 'Belgium', 'BE'),
                (22, 'Belize', 'BZ'),
                (23, 'Benin', 'BJ'),
                (24, 'Bermuda', 'BM'),
                (25, 'Bhutan', 'BT'),
                (26, 'Bolivia', 'BO'),
                (27, 'Bosnia and Herzegovina', 'BA'),
                (28, 'Botswana', 'BW'),
                (29, 'Bouvet Island', 'BV'),
                (30, 'Brazil', 'BR'),
                (31, 'British Indian Ocean Territory', 'IO'),
                (32, 'Brunei Darussalam', 'BN'),
                (33, 'Bulgaria', 'BG'),
                (34, 'Burkina Faso', 'BF'),
                (35, 'Burundi', 'BI'),
                (36, 'Cambodia', 'KH'),
                (37, 'Cameroon', 'CM'),
                (38, 'Canada', 'CA'),
                (39, 'Cape Verde', 'CV'),
                (40, 'Cayman Islands', 'KY'),
                (41, 'Central African Republic', 'CF'),
                (42, 'Chad', 'TD'),
                (43, 'Chile', 'CL'),
                (44, 'China', 'CN'),
                (45, 'Christmas Island', 'CX'),
                (46, 'Cocos (Keeling) Islands', 'CC'),
                (47, 'Colombia', 'CO'),
                (48, 'Comoros', 'KM'),
                (49, 'Congo', 'CG'),
                (50, 'Cook Islands', 'CK'),
                (51, 'Costa Rica', 'CR'),
                (52, 'Croatia (Hrvatska)', 'HR'),
                (53, 'Cuba', 'CU'),
                (54, 'Cyprus', 'CY'),
                (55, 'Czech Republic', 'CZ'),
                (56, 'Denmark', 'DK'),
                (57, 'Djibouti', 'DJ'),
                (58, 'Dominica', 'DM'),
                (59, 'Dominican Republic', 'DO'),
                (60, 'East Timor', 'TP'),
                (61, 'Ecuador', 'EC'),
                (62, 'Egypt', 'EG'),
                (63, 'El Salvador', 'SV'),
                (64, 'Equatorial Guinea', 'GQ'),
                (65, 'Eritrea', 'ER'),
                (66, 'Estonia', 'EE'),
                (67, 'Ethiopia', 'ET'),
                (68, 'Falkland Islands (Malvinas)', 'FK'),
                (69, 'Faroe Islands', 'FO'),
                (70, 'Fiji', 'FJ'),
                (71, 'Finland', 'FI'),
                (72, 'France', 'FR'),
                (73, 'France, Metropolitan', 'FX'),
                (74, 'French Guiana', 'GF'),
                (75, 'French Polynesia', 'PF'),
                (76, 'French Southern Territories', 'TF'),
                (77, 'Gabon', 'GA'),
                (78, 'Gambia', 'GM'),
                (79, 'Georgia', 'GE'),
                (80, 'Germany', 'DE'),
                (81, 'Ghana', 'GH'),
                (82, 'Gibraltar', 'GI'),
                (83, 'Guernsey', 'GG'),
                (84, 'Greece', 'GR'),
                (85, 'Greenland', 'GL'),
                (86, 'Grenada', 'GD'),
                (87, 'Guadeloupe', 'GP'),
                (88, 'Guam', 'GU'),
                (89, 'Guatemala', 'GT'),
                (90, 'Guinea', 'GN'),
                (91, 'Guinea-Bissau', 'GW'),
                (92, 'Guyana', 'GY'),
                (93, 'Haiti', 'HT'),
                (94, 'Heard and Mc Donald Islands', 'HM'),
                (95, 'Honduras', 'HN'),
                (96, 'Hong Kong', 'HK'),
                (97, 'Hungary', 'HU'),
                (98, 'Iceland', 'IS'),
                (99, 'India', 'IN'),
                (100, 'Isle of Man', 'IM'),
                (101, 'Indonesia', 'ID'),
                (102, 'Iran (Islamic Republic of)', 'IR'),
                (103, 'Iraq', 'IQ'),
                (104, 'Ireland', 'IE'),
                (105, 'Israel', 'IL'),
                (106, 'Italy', 'IT'),
                (107, 'Ivory Coast', 'CI'),
                (108, 'Jersey', 'JE'),
                (109, 'Jamaica', 'JM'),
                (110, 'Japan', 'JP'),
                (111, 'Jordan', 'JO'),
                (112, 'Kazakhstan', 'KZ'),
                (113, 'Kenya', 'KE'),
                (114, 'Kiribati', 'KI'),
                (115, 'Korea, Democratic People\'s Republic of', 'KP'),
                (116, 'Korea, Republic of', 'KR'),
                (117, 'Kosovo', 'XK'),
                (118, 'Kuwait', 'KW'),
                (119, 'Kyrgyzstan', 'KG'),
                (120, 'Lao People\'s Democratic Republic', 'LA'),
                (121, 'Latvia', 'LV'),
                (122, 'Lebanon', 'LB'),
                (123, 'Lesotho', 'LS'),
                (124, 'Liberia', 'LR'),
                (125, 'Libyan Arab Jamahiriya', 'LY'),
                (126, 'Liechtenstein', 'LI'),
                (127, 'Lithuania', 'LT'),
                (128, 'Luxembourg', 'LU'),
                (129, 'Macau', 'MO'),
                (130, 'Macedonia', 'MK'),
                (131, 'Madagascar', 'MG'),
                (132, 'Malawi', 'MW'),
                (133, 'Malaysia', 'MY'),
                (134, 'Maldives', 'MV'),
                (135, 'Mali', 'ML'),
                (136, 'Malta', 'MT'),
                (137, 'Marshall Islands', 'MH'),
                (138, 'Martinique', 'MQ'),
                (139, 'Mauritania', 'MR'),
                (140, 'Mauritius', 'MU'),
                (141, 'Mayotte', 'YT'),
                (142, 'Mexico', 'MX'),
                (143, 'Micronesia, Federated States of', 'FM'),
                (144, 'Moldova, Republic of', 'MD'),
                (145, 'Monaco', 'MC'),
                (146, 'Mongolia', 'MN'),
                (147, 'Montenegro', 'ME'),
                (148, 'Montserrat', 'MS'),
                (149, 'Morocco', 'MA'),
                (150, 'Mozambique', 'MZ'),
                (151, 'Myanmar', 'MM'),
                (152, 'Namibia', 'NA'),
                (153, 'Nauru', 'NR'),
                (154, 'Nepal', 'NP'),
                (155, 'Netherlands', 'NL'),
                (156, 'Netherlands Antilles', 'AN'),
                (157, 'New Caledonia', 'NC'),
                (158, 'New Zealand', 'NZ'),
                (159, 'Nicaragua', 'NI'),
                (160, 'Niger', 'NE'),
                (161, 'Nigeria', 'NG'),
                (162, 'Niue', 'NU'),
                (163, 'Norfolk Island', 'NF'),
                (164, 'Northern Mariana Islands', 'MP'),
                (165, 'Norway', 'NO'),
                (166, 'Oman', 'OM'),
                (167, 'Pakistan', 'PK'),
                (168, 'Palau', 'PW'),
                (169, 'Palestine', 'PS'),
                (170, 'Panama', 'PA'),
                (171, 'Papua New Guinea', 'PG'),
                (172, 'Paraguay', 'PY'),
                (173, 'Peru', 'PE'),
                (174, 'Philippines', 'PH'),
                (175, 'Pitcairn', 'PN'),
                (176, 'Poland', 'PL'),
                (177, 'Portugal', 'PT'),
                (178, 'Puerto Rico', 'PR'),
                (179, 'Qatar', 'QA'),
                (180, 'Reunion', 'RE'),
                (181, 'Romania', 'RO'),
                (182, 'Russian Federation', 'RU'),
                (183, 'Rwanda', 'RW'),
                (184, 'Saint Kitts and Nevis', 'KN'),
                (185, 'Saint Lucia', 'LC'),
                (186, 'Saint Vincent and the Grenadines', 'VC'),
                (187, 'Samoa', 'WS'),
                (188, 'San Marino', 'SM'),
                (189, 'Sao Tome and Principe', 'ST'),
                (190, 'Saudi Arabia', 'SA'),
                (191, 'Senegal', 'SN'),
                (192, 'Serbia', 'RS'),
                (193, 'Seychelles', 'SC'),
                (194, 'Sierra Leone', 'SL'),
                (195, 'Singapore', 'SG'),
                (196, 'Slovakia', 'SK'),
                (197, 'Slovenia', 'SI'),
                (198, 'Solomon Islands', 'SB'),
                (199, 'Somalia', 'SO'),
                (200, 'South Africa', 'ZA'),
                (201, 'South Georgia South Sandwich Islands', 'GS'),
                (202, 'Spain', 'ES'),
                (203, 'Sri Lanka', 'LK'),
                (204, 'St. Helena', 'SH'),
                (205, 'St. Pierre and Miquelon', 'PM'),
                (206, 'Sudan', 'SD'),
                (207, 'Suriname', 'SR'),
                (208, 'Svalbard and Jan Mayen Islands', 'SJ'),
                (209, 'Swaziland', 'SZ'),
                (210, 'Sweden', 'SE'),
                (211, 'Switzerland', 'CH'),
                (212, 'Syrian Arab Republic', 'SY'),
                (213, 'Taiwan', 'TW'),
                (214, 'Tajikistan', 'TJ'),
                (215, 'Tanzania, United Republic of', 'TZ'),
                (216, 'Thailand', 'TH'),
                (217, 'Togo', 'TG'),
                (218, 'Tokelau', 'TK'),
                (219, 'Tonga', 'TO'),
                (220, 'Trinidad and Tobago', 'TT'),
                (221, 'Tunisia', 'TN'),
                (222, 'Turkey', 'TR'),
                (223, 'Turkmenistan', 'TM'),
                (224, 'Turks and Caicos Islands', 'TC'),
                (225, 'Tuvalu', 'TV'),
                (226, 'Uganda', 'UG'),
                (227, 'Ukraine', 'UA'),
                (228, 'United Arab Emirates', 'AE'),
                (229, 'United Kingdom', 'GB'),
                (230, 'United States', 'US'),
                (231, 'United States minor outlying islands', 'UM'),
                (232, 'Uruguay', 'UY'),
                (233, 'Uzbekistan', 'UZ'),
                (234, 'Vanuatu', 'VU'),
                (235, 'Vatican City State', 'VA'),
                (236, 'Venezuela', 'VE'),
                (237, 'Vietnam', 'VN'),
                (238, 'Virgin Islands (British)', 'VG'),
                (239, 'Virgin Islands (U.S.)', 'VI'),
                (240, 'Wallis and Futuna Islands', 'WF'),
                (241, 'Western Sahara', 'EH'),
                (242, 'Yemen', 'YE'),
                (243, 'Yugoslavia', 'YU'),
                (244, 'Zaire', 'ZR'),
                (245, 'Zambia', 'ZM'),
                (246, 'Zimbabwe', 'ZW'),
                (247, 'Bonaire, Sint Eustatius and Saba', 'BQ'),
                (251, 'Timor-Leste', 'TL')");
        $this->addSql('INSERT INTO `users` (`id`, `country_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `birthday`, `gender`, `sign_up_date`, `deleted_at`) VALUES (1, 1, \'admin@admin.pl\', \'admin@admin.pl\', \'admin@admin.pl\', \'admin@admin.pl\', 1, NULL, \'$2y$13$9Ssqowj.rIe8tspe1HJ.tOfWCsKYzpFChLomqKp/Ruk5nejebNFHm\', \'2017-05-29 14:39:49\', NULL, NULL, \'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}\', \'1988-11-01\', 1, \'2017-05-18 16:19:13\', NULL),(2, 1, \'default@default.pl\', \'default@default.pl\', \'default@default.pl\', \'default@default.pl\', 1, NULL, \'$2y$13$4LQtFWCNNOdd1g3PqEzyXuOlejfI7pEBRx2d8HjNsJtM3.wKmQ5m6\', NULL, NULL, NULL, \'a:0:{}\', NULL, 0, \'2017-05-29 14:49:19\', NULL);');
        $this->addSql('INSERT INTO `clients` (`id`, `random_id`, `redirect_uris`, `secret`, `allowed_grant_types`) VALUES (1, \'5xnlgwlwqf8k80s0o0k880o848gws4co80gw0ososkgs40scwo\', \'a:0:{}\', \'33zhl6tlg544o40co4ss8k8cc0w0sgk80cwc0kkw0w4sos0w0k\', \'a:4:{i:0;s:8:\"password\";i:1;s:18:\"client_credentials\";i:2;s:12:\"access_token\";i:3;s:13:\"refresh_token\";}\');');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('TRUNCATE TABLE countries');
        $this->addSql('TRUNCATE TABLE users');
        $this->addSql('TRUNCATE TABLE clients');
        $this->addSql('ALTER TABLE access_tokens DROP FOREIGN KEY FK_58D184BC19EB6921');
        $this->addSql('ALTER TABLE auth_codes DROP FOREIGN KEY FK_298F903819EB6921');
        $this->addSql('ALTER TABLE refresh_tokens DROP FOREIGN KEY FK_9BACE7E119EB6921');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9F92F3E70');
        $this->addSql('ALTER TABLE access_tokens DROP FOREIGN KEY FK_58D184BCA76ED395');
        $this->addSql('ALTER TABLE auth_codes DROP FOREIGN KEY FK_298F9038A76ED395');
        $this->addSql('ALTER TABLE refresh_tokens DROP FOREIGN KEY FK_9BACE7E1A76ED395');
        $this->addSql('DROP TABLE access_tokens');
        $this->addSql('DROP TABLE auth_codes');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE countries');
        $this->addSql('DROP TABLE users');
    }
}
