<?php

passthru(sprintf('php %s/../bin/console cache:clear --env=test --no-warmup -q', __DIR__));
passthru(sprintf('php %s/../bin/console doctrine:database:drop --if-exists --force --env=test -q', __DIR__));
passthru(sprintf('php %s/../bin/console doctrine:database:create --if-not-exists --env=test -q', __DIR__));
passthru(sprintf('php %s/../bin/console doctrine:migrations:migrate --allow-no-migration --env=test --no-interaction -q', __DIR__));
//passthru(sprintf('php %s/../bin/console doctrine:fixtures:load --append --env=test --no-interaction -q', __DIR__)); //comment if there is no any fixtures

require __DIR__.'/../vendor/autoload.php';
