<?php

namespace Tests;

use Doctrine\Bundle\DoctrineBundle\Registry;
use FOS\OAuthServerBundle\Entity\ClientManager;
use OAuth2\OAuth2;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as WebTest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use UserBundle\Entity\User;

/**
 * Class WebTestCase.
 */
class WebTestCase extends WebTest
{
    const CLIENT_ID = '1_5xnlgwlwqf8k80s0o0k880o848gws4co80gw0ososkgs40scwo';
    const USER_ADMIN_ID = 1;
    const REGULAR_USER_ID = 2;

    /**
     * @var array
     */
    protected $adminCredentials;

    /**
     * @var array
     */
    protected $userCredentials;

    /**
     * @var Client
     */
    protected $client;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->client = self::createClient();
        $client = $this->getFosOAuthClientManager()->findClientByPublicId(self::CLIENT_ID);
        $this->adminCredentials = $this->getFosOAuthServer()->createAccessToken($client, $this->getUser(self::USER_ADMIN_ID));
        $this->userCredentials = $this->getFosOAuthServer()->createAccessToken($client, $this->getUser(self::REGULAR_USER_ID));
    }

    /**
     * @return array
     */
    protected function getHeaders()
    {
        return [
            'HTTP_CONTENT_TYPE' => 'application/json',
        ];
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function addAdminRequestToken($url)
    {
        return $url.'?access_token='.$this->adminCredentials['access_token'];
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function addUserRequestToken($url)
    {
        return $url.'?access_token='.$this->userCredentials['access_token'];
    }

    /**
     * @param int $userId
     *
     * @return User|null
     */
    private function getUser(int $userId): ?User
    {
        return $this->getDoctrineRegistry()->getManager()->getRepository('UserBundle:User')->find($userId);
    }

    /**
     * @return object|Registry
     */
    private function getDoctrineRegistry(): Registry
    {
        return $this->getContainer()->get('doctrine');
    }

    /**
     * @return object|ClientManager
     */
    private function getFosOAuthClientManager(): ClientManager
    {
        return $this->getContainer()->get('fos_oauth_server.client_manager.default');
    }

    /**
     * @return object|OAuth2
     */
    private function getFosOAuthServer(): OAuth2
    {
        return $this->getContainer()->get('fos_oauth_server.server');
    }

    /**
     * @return ContainerInterface
     */
    private function getContainer(): ContainerInterface
    {
        $container = $this->client->getContainer();
        if ($container instanceof ContainerInterface) {
            return $container;
        }

        throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'Kernel has been shutdown or not started yet.');
    }
}
