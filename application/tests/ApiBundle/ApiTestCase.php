<?php

namespace Tests\ApiBundle;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as WebTest;
use Symfony\Component\DomCrawler\Crawler;
use UserBundle\Entity\User;

/**
 * @covers \ApiBundle\Entity\AccessToken
 * @covers \ApiBundle\Entity\AuthCode
 * @covers \ApiBundle\Entity\Client
 * @covers \ApiBundle\Entity\RefreshToken
 */
class ApiTestCase extends WebTest
{
    const CLIENT_ID = '1_5xnlgwlwqf8k80s0o0k880o848gws4co80gw0ososkgs40scwo';
    const USER_ADMIN_ID = 1;
    const REGULAR_USER_ID = 2;

    /**
     * @var array
     */
    protected $adminCredentials;

    /**
     * @var array
     */
    protected $userCredentials;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $apiUrl;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->client = self::createClient();
        $container = $this->client->getContainer();
        $fosOAuthServer = $container->get('fos_oauth_server.server');

        $client = $container->get('fos_oauth_server.client_manager.default')->findClientByPublicId(self::CLIENT_ID);
        $this->adminCredentials = $fosOAuthServer->createAccessToken($client, $this->getUser(self::USER_ADMIN_ID), 'api');
        $this->userCredentials = $fosOAuthServer->createAccessToken($client, $this->getUser(self::REGULAR_USER_ID));

        $this->apiUrl = sprintf('/api/%s', $container->getParameter('api_version'));
    }

    /**
     * @param string $verb
     * @param string $endpoint
     * @param array  $data
     *
     * @return Crawler
     */
    public function jsonRequest($verb, $endpoint, array $data = array())
    {
        if (!empty($data)) {
            $data = json_encode($data);
        }

        return $this->client->request($verb, sprintf('%s%s', $this->apiUrl, $endpoint),
            array(),
            array(),
            array(
                'ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            ),
            $data
        );
    }

    /**
     * @return array
     */
    public function getResponseJsonContent()
    {
        $response = $this->client->getResponse();

        return json_decode($response->getContent(), true);
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function addAdminRequestToken($url)
    {
        return $url.'?'.http_build_query(['access_token' => $this->adminCredentials['access_token']]);
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function addUserRequestToken($url)
    {
        return $url.'?'.http_build_query(['access_token' => $this->userCredentials['access_token']]);
    }

    /**
     * @param int $userId
     *
     * @return User
     */
    private function getUser(int $userId)
    {
        return $this->client->getContainer()->get('doctrine')->getManager()->getRepository('UserBundle:User')->find($userId);
    }
}
