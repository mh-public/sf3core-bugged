<?php

namespace Tests\ApiBundle\Controller;

use Tests\ApiBundle\ApiTestCase;

/**
 * @covers \ApiBundle\Controller\CountryController
 */
class CountryControllerTest extends ApiTestCase
{
    /**
     * @param array $url
     *
     * @dataProvider urlProvider
     */
    public function testGetCountry($url)
    {
        $url .= '/1';
        $url = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @param array $url
     *
     * @dataProvider urlProvider
     */
    public function testCountrySecurity($url)
    {
        $url .= '/1';
        $url = $this->addUserRequestToken($url);
        $this->jsonRequest('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @param array|string $url
     *
     * @dataProvider urlProvider
     */
    public function testGetCountries($url)
    {
        $url = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @param array|string $url
     *
     * @dataProvider urlProvider
     */
    public function testGetCountriesSecurity($url)
    {
        $url = $this->addUserRequestToken($url);
        $this->jsonRequest('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/countries'],
        ];
    }
}
