<?php

namespace Tests\ApiBundle\Controller;

use Tests\ApiBundle\ApiTestCase;

/**
 * @covers \ApiBundle\Controller\UserController
 * @covers \UserBundle\Security\UserVoter
 */
class UserControllerTest extends ApiTestCase
{
    /**
     * @param array $url
     *
     * @dataProvider urlProvider
     */
    public function testGetUser($url)
    {
        $url .= '/1';
        $url = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

//    /**
//     * @param array $url
//     *
//     * @dataProvider urlProvider
//     */
//    public function testGetUserSecurity($url)
//    {
//        $url .= '/1';
//        $url = $this->addUserRequestToken($url);
//        $this->jsonRequest('GET', $url);
//
//        $this->assertTrue($this->client->getResponse()->isForbidden());
//    }

    /**
     * @param array|string $url
     *
     * @dataProvider urlProvider
     */
    public function testGetUsers($url)
    {
        $url = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @param string $url
     * @param array  $data
     * @param int    $expectedCode
     *
     * @dataProvider postProvider
     */
    public function testCreateUser($url, $data, $expectedCode)
    {
        $url = $this->addAdminRequestToken($url);
        $this->jsonRequest('POST', $url, $data);

        $this->assertSame($expectedCode, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @param array|string $url
     *
     * @dataProvider postProvider
     */
    public function testCreateUserSecurity($url)
    {
        $url = $this->addUserRequestToken($url);
        $this->jsonRequest('POST', $url, []);

        $this->assertTrue($this->client->getResponse()->isForbidden());
    }

    /**
     * @param string $url
     * @param array  $data
     * @param int    $expectedCode
     *
     * @dataProvider updateProvider
     */
    public function testUpdateUser($url, $data, $expectedCode)
    {
        $usersUrl = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $usersUrl);

        $usersResponse = $this->getResponseJsonContent();
        $lastUser = end($usersResponse['data']['items']);

        $updateUrl = $this->addAdminRequestToken($url.'/'.$lastUser['id']);
        $this->jsonRequest('PUT', $updateUrl, $data);
        $response = $this->getResponseJsonContent();

        if (200 === $expectedCode) {
            $this->assertContains('test4', $response['data']);
            $this->assertContains('test4@test3.pl', $response['data']);
        }
        $this->assertSame($expectedCode, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @param string $url
     *
     * @dataProvider urlProvider
     */
    public function testDeleteUser($url)
    {
        $usersUrl = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $usersUrl);

        $usersResponse = $this->getResponseJsonContent();
        $lastUser = end($usersResponse['data']['items']);

        $deleteUrl = $this->addAdminRequestToken($url.'/'.$lastUser['id']);
        $this->jsonRequest('DELETE', $deleteUrl);

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    /**
     * @param string $url
     *
     * @dataProvider urlProvider
     */
    public function testDeleteUserNotFound($url)
    {
        $usersUrl = $this->addAdminRequestToken($url);
        $this->jsonRequest('GET', $usersUrl);

        $deleteUrl = $this->addAdminRequestToken($url.'/0');
        $this->jsonRequest('DELETE', $deleteUrl);

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/users'],
        ];
    }

    /**
     * @return array
     */
    public function postProvider()
    {
        return [
            [
                '/users',
                [
                    'user_bundle_user_type' => [
                        'username' => 'testCreated',
                        'email' => 'testCreated@test3.pl',
                        'country' => 5,
                        'gender' => 1,
                        'roles' => 'ROLE_USER',
                        'enabled' => 1,
                        'birthday' => '1988-01-01',
                        'plainPassword' => [
                            'first' => 'Admin1234',
                            'second' => 'Admin1234',
                        ],
                    ],
                ],
                201,
            ],
            [
                '/users',
                [
                    'user_bundle_user_type' => [
                        'country' => 5,
                        'gender' => 1,
                        'enabled' => true,
                        'birthday' => '1988-01-01',
                        'plainPassword' => [
                            'first' => 'Admin1234',
                            'second' => 'Admin1234',
                        ],
                    ],
                ],
                400,
            ],
        ];
    }

    /**
     * @return array
     */
    public function updateProvider()
    {
        return [
            [
                '/users',
                [
                    'user_bundle_user_type' => [
                        'username' => 'test4',
                        'email' => 'test4@test3.pl',
                    ],
                ],
                200,
            ],
            [
                '/users',
                [
                    'user_bundle_user_type' => [
                        'username' => 'test4',
                        'email' => 'test4@test3.pl',
                        'country' => 6,
                        'gender' => 4,
                        'birthday' => '1988-01-',
                    ],
                ],
                400,
            ],
        ];
    }
}
