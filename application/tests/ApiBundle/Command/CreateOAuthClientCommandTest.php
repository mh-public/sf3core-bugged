<?php

namespace Tests\ApiBundle\Command;

use ApiBundle\Command\CreateOAuthClientCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @covers \ApiBundle\Command\CreateOAuthClientCommand
 */
class CreateOAuthClientCommandTest extends TestCase
{
    public function testExecute()
    {
        $redirectUris = ['www.test.com'];
        $grantTypes = ['password', 'client_credentials'];
        $commandTester = $this->createCommandTester($this->getContainer($redirectUris, $grantTypes));
        $exitCode = $commandTester->execute([
            '--grant-type' => $grantTypes,
            '--redirect-uri' => $redirectUris,
        ], [
            'decorated' => false,
            'interactive' => false,
        ]);

        $this->assertSame(0, $exitCode, 'Returns 0 in case of success');
        $this->assertRegExp('/Added a new client with public id , secret/', $commandTester->getDisplay());
    }

    /**
     * @param ContainerInterface $container
     * @param Application|null   $application
     *
     * @return CommandTester
     */
    private function createCommandTester(ContainerInterface $container, Application $application = null)
    {
        if (null === $application) {
            $application = new Application();
        }

        $application->setAutoExit(false);
        $command = new CreateOAuthClientCommand();
        $command->setContainer($container);

        $application->add($command);

        return new CommandTester($application->find('oauth-server:client:create'));
    }

    /**
     * @param array $redirectUri
     * @param array $grantTypes
     *
     * @return ContainerInterface
     */
    private function getContainer(array $redirectUri, array $grantTypes)
    {
        $container = $this->createMock('Symfony\Component\DependencyInjection\ContainerInterface');
        $client = $this->createMock('FOS\OAuthServerBundle\Model\ClientInterface');

        $manipulator = $this->createMock('ApiBundle\Util\ClientManipulator');
        $manipulator
            ->expects($this->once())
            ->method('create')
            ->with($redirectUri, $grantTypes)
            ->will($this->returnValue($client));

        $container
            ->expects($this->once())
            ->method('get')
            ->with('api.util.client_manipulator')
            ->will($this->returnValue($manipulator));

        return $container;
    }
}
