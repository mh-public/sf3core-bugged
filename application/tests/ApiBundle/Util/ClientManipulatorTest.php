<?php

namespace Tests\ApiBundle\Util;

use ApiBundle\Entity\Client;
use ApiBundle\Util\ClientManipulator;
use PHPUnit\Framework\TestCase;

/**
 * Class ClientManipulatorTest.
 *
 * @covers \ApiBundle\Util\ClientManipulator
 */
class ClientManipulatorTest extends TestCase
{
    public function testCreate()
    {
        $clientManagerMock = $this->createMock('FOS\OAuthServerBundle\Entity\ClientManager');
        $client = new Client();
        $redirectUri = ['www.test.com'];
        $grantTypes = ['password', 'client_credentials'];
        $clientManagerMock->expects($this->once())
            ->method('createClient')
            ->will($this->returnValue($client));
        $clientManagerMock->expects($this->once())
            ->method('updateClient')
            ->will($this->returnValue($client))
            ->with($this->isInstanceOf('ApiBundle\Entity\Client'));

        $manipulator = new ClientManipulator($clientManagerMock);
        $manipulator->create($redirectUri, $grantTypes);

        $this->assertSame($redirectUri, $client->getRedirectUris());
        $this->assertSame($grantTypes, $client->getAllowedGrantTypes());
        $this->assertNotNull($client->getPublicId());
        $this->assertNotNull($client->getSecret());
    }
}
