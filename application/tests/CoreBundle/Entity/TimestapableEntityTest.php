<?php

namespace Tests\CoreBundle\Entity;

use CoreBundle\Entity\TimestapableEntity;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use UserBundle\Entity\User;

/**
 * Class TimestapableEntityTest.
 *
 * @cover \CoreBundle\Entity\TimestapableEntity
 */
class TimestapableEntityTest extends TestCase
{
    /**
     * @expectedException \TypeError
     */
    public function testIsDateCreatedNull()
    {
        $entity = $this->getTimestapableEntity();
        $this->assertNull($entity->getDateCreated());

        $dateCreated = new \DateTime();

        $entity->setDateCreated($dateCreated);
        $this->assertSame($dateCreated, $entity->getDateCreated());
    }

    public function testDateCreated()
    {
        $entity = $this->getTimestapableEntity();
        $dateCreated = new \DateTime();

        $entity->setDateCreated($dateCreated);
        $this->assertSame($dateCreated, $entity->getDateCreated());
    }

    /**
     * @expectedException \TypeError
     */
    public function testIsCreatedByNull()
    {
        $entity = $this->getTimestapableEntity();
        $this->assertNull($entity->getCreatedBy());

        $createBy = new User();
        $entity->setCreatedBy($createBy);
        $this->assertSame($createBy, $entity->getCreatedBy());
    }

    public function testCreatedBy()
    {
        $entity = $this->getTimestapableEntity();

        $createBy = new User();
        $entity->setCreatedBy($createBy);
        $this->assertSame($createBy, $entity->getCreatedBy());
    }

    public function testDateModified()
    {
        $entity = $this->getTimestapableEntity();
        $this->assertNull($entity->getDateModified());

        $dateModified = new \DateTime();

        $entity->setDateModified($dateModified);
        $this->assertSame($dateModified, $entity->getDateModified());
    }

    public function testModifiedBy()
    {
        $entity = $this->getTimestapableEntity();
        $this->assertNull($entity->getModifiedBy());

        $modifiedBy = new User();

        $entity->setModifiedBy($modifiedBy);
        $this->assertSame($modifiedBy, $entity->getModifiedBy());
    }

    /**
     * @return TimestapableEntity|MockObject
     */
    protected function getTimestapableEntity()
    {
        try {
            return $this->getMockForAbstractClass(TimestapableEntity::class);
        } catch (\ReflectionException $exception) {
        }
    }
}
