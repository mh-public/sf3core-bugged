<?php

namespace Tests\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;

/**
 * Class DashboardControllerTest.
 *
 * @covers \CoreBundle\Controller\DashboardController
 */
class DashboardControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@admin.pl',
            'PHP_AUTH_PW' => 'admin',
        ]);
    }

    public function testDashboardAction()
    {
        $crawler = $this->client->request('GET', '/admin/dashboard');
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertEquals(
            'Dashboard',
            $crawler->filter('.content > h1')->text()
        );
    }
}
