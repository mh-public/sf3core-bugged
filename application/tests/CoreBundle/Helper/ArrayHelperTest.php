<?php

namespace CoreBundle\Helper;

use CoreBundle\Helpers\ArrayHelper;
use PHPUnit\Framework\TestCase;

/**
 * @covers \CoreBundle\Helpers\ArrayHelper
 */
class ArrayHelperTest extends TestCase
{
    /**
     * @param array $array
     * @param array $arrayKeyMap
     * @dataProvider changeArrayKeyProvider
     */
    public function testChangeArrayKeys($array, $arrayKeyMap)
    {
        $arrayHelper = new ArrayHelper();
        $result = $arrayHelper->changeArrayKeys($array, $arrayKeyMap);

        foreach ($result as $key => $item) {
            $isset = in_array($key, $arrayKeyMap);
            if (!$isset) {
                $this->assertFalse($isset);
                continue;
            }

            $this->assertTrue($isset);
        }
    }

    /**
     * @return array
     */
    public function changeArrayKeyProvider()
    {
        return [
            [
                ['key' => 'value', 'key1' => 'value', 'key2' => 'value'], ['key' => 'keynew', 'key1' => 'key1new', 'key4' => 'key2new'],
            ],
        ];
    }
}
