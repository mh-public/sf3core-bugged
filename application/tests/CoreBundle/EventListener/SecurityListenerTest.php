<?php

namespace Tests\CoreBundle\EventListener;

use CoreBundle\EventListener\SecurityListener;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Twig\Error\Error;
use UserBundle\Entity\User;

class SecurityListenerTest extends TestCase
{
    /**
     * @var SecurityListener
     */
    private $securityListener;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var TokenInterface
     */
    private $tokenInerface;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var TokenStorage
     */
    private $token;

    /**
     * @var UsernamePasswordToken
     */
    private $userPasswordToken;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var User
     */
    private $user;

    /**
     * @var CsrfTokenManager
     */
    private $csrfTokenManager;

    /**
     * @var TwigEngine
     */
    private $twigEngine;

    public function setUp()
    {
        $this->request = $this->createMock(Request::class);
        $this->tokenInerface = $this->createMock(TokenInterface::class);
        $this->userPasswordToken = $this->createMock(UsernamePasswordToken::class);
        $this->user = $this->createMock(User::class);
        $this->user->expects($this->any())
            ->method('getRoles')
            ->will($this->returnValue([User::ROLE_SUPER_ADMIN]));

        $this->router = $this->createMock(UrlGeneratorInterface::class);
        $this->session = $this->createMock(Session::class);
        $this->twigEngine = $this->createMock(TwigEngine::class);
        $this->token = $this->createMock(TokenStorage::class);
        $this->csrfTokenManager = $this->createMock(CsrfTokenManager::class);

        $this->securityListener = new SecurityListener($this->router, $this->session, $this->twigEngine, $this->token, $this->csrfTokenManager);

        parent::setUp();
    }

    public function testOnAuthenticationSuccessException()
    {
        $this->router->expects($this->once())
            ->method('generate')
            ->with($this->equalTo('dashboard'))
            ->will($this->returnValue('/admin/dashboard'));

        $this->expectException(AuthenticationException::class);
        $this->securityListener->onAuthenticationSuccess($this->request, $this->tokenInerface);
    }

    public function testOnAuthenticationSuccess()
    {
        $this->router->expects($this->any())
            ->method('generate')
            ->with($this->equalTo('dashboard'))
            ->will($this->returnValue('/admin/dashboard'));

        $this->token->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue($this->userPasswordToken));

        $this->userPasswordToken->expects($this->once())
            ->method('getUser')
            ->will($this->returnValue($this->user));

        $this->session->expects($this->any())
            ->method('get')
            ->with($this->equalTo('_security.main.target_path'))
            ->will($this->returnValue(true));

        $this->securityListener->onAuthenticationSuccess($this->request, $this->tokenInerface);
    }

    public function testOnLogoutSuccess()
    {
        $this->router->expects($this->once())
            ->method('generate')
            ->with($this->equalTo('fos_user_security_login'))
            ->will($this->returnValue('test'));

        $this->assertInstanceOf(RedirectResponse::class, $this->securityListener->onLogoutSuccess($this->request));
    }

    /**
     * @throws Error
     */
    public function testOnAuthenticationFailure()
    {
        $csrfToken = $this->createMock(CsrfToken::class);
        $this->csrfTokenManager->expects($this->once())
            ->method('getToken')
            ->with($this->equalTo('authenticate'))
            ->will($this->returnValue($csrfToken));
        $authenticationException = $this->createMock(AuthenticationException::class);

        $params = [
            'error' => $authenticationException,
            'csrf_token' => null,
            'last_username' => null,
        ];
        $this->twigEngine->expects($this->once())
            ->method('renderResponse')
            ->with($this->equalTo('FOSUserBundle:Security:login.html.twig'), $this->equalTo($params))
            ->will($this->returnValue(new Response()));

        $result = $this->securityListener->onAuthenticationFailure($this->request, $authenticationException);

        $this->assertInstanceOf(Response::class, $result);
    }
}
