<?php

namespace Tests\CoreBundle\EventListener\Entity;

use CoreBundle\EventListener\Entity\TimestampableInterface;
use CoreBundle\EventListener\Entity\TimestampableSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class TimestampableSubscriberTest extends TestCase
{
    /**
     * @var TimestampableSubscriber
     */
    private $timestampableSubscriber;

    /**
     * @var LifecycleEventArgs
     */
    private $args;

    /**
     * @var TimestampableInterface
     */
    private $entity;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @throws \ReflectionException
     */
    public function setUp()
    {
        $this->date = $this->createMock(\DateTime::class);
        $this->date = new \DateTime();

        $this->entity = $this->createMock(TimestampableInterface::class);
        $this->entity->expects($this->any())
            ->method('setDateCreated');
        $this->entity->expects($this->any())
            ->method('setDateModified');

        $this->args = $this->createMock(LifecycleEventArgs::class);
        $this->args->expects($this->any())
            ->method('getEntity')
            ->will($this->returnValue($this->entity));

        $this->timestampableSubscriber = new TimestampableSubscriber();

        parent::setUp();
    }

    public function testGetSubscribedEvents()
    {
        $result = $this->timestampableSubscriber->getSubscribedEvents();

        $this->assertTrue(in_array('prePersist', $result));
        $this->assertTrue(in_array('preUpdate', $result));
    }

    public function testPrePersist()
    {
        $this->timestampableSubscriber->prePersist($this->args);

        $this->addToAssertionCount(1);
    }

    public function testPreUpdate()
    {
        $this->timestampableSubscriber->preUpdate($this->args);

        $this->addToAssertionCount(1);
    }
}
