<?php

namespace Tests\CoreBundle\EventListener\Entity;

use CoreBundle\EventListener\Entity\UserstampableInterface;
use CoreBundle\EventListener\Entity\UserstampableSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use UserBundle\Entity\User;

class UserstampableSubscriberTest extends TestCase
{
    /**
     * @var UserstampableSubscriber
     */
    private $userstamableSubscriber;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var LifecycleEventArgs
     */
    private $args;

    /**
     * @var UserstampableInterface
     */
    private $entity;

    /**
     * @var TokenInterface
     */
    private $tokenInterface;

    /**
     * @throws \ReflectionException
     */
    public function setUp()
    {
        $this->entity = $this->createMock(UserstampableInterface::class);
        $this->entity->expects($this->any())
            ->method('setCreatedBy');
        $this->entity->expects($this->any())
            ->method('setModifiedBy');

        $this->args = $this->createMock(LifecycleEventArgs::class);
        $this->args->expects($this->any())
            ->method('getEntity')
            ->will($this->returnValue($this->entity));

        $this->tokenStorage = $this->createMock(TokenStorageInterface::class);
        $this->tokenInterface = $this->createMock(TokenInterface::class);

        $this->userstamableSubscriber = new UserstampableSubscriber($this->tokenStorage);
        parent::setUp();
    }

    public function testGetSubscribedEvents()
    {
        $result = $this->userstamableSubscriber->getSubscribedEvents();

        $this->assertTrue(in_array('prePersist', $result));
        $this->assertTrue(in_array('preUpdate', $result));
    }

    public function testPrePersistException()
    {
        $this->expectException(AuthenticationException::class);
        $this->userstamableSubscriber->prePersist($this->args);
    }

    public function testPrePersist()
    {
        $this->tokenInterface->expects($this->once())
            ->method('getuser')
            ->will($this->returnValue(new User()));
        $this->tokenStorage->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue($this->tokenInterface));

        $this->userstamableSubscriber->prePersist($this->args);
    }

    public function testPreUpdate()
    {
        $this->tokenInterface->expects($this->once())
            ->method('getuser')
            ->will($this->returnValue(new User()));
        $this->tokenStorage->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue($this->tokenInterface));

        $this->userstamableSubscriber->preUpdate($this->args);
    }
}
