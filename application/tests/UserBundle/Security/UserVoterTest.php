<?php

namespace UserBundle\Security;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use UserBundle\Entity\User;

class UserVoterTest extends TestCase
{
    public function testVoteOnAttributeNoCurrentUser()
    {
        $userVoter = new UserVoter($this->getMockDecisionManager());
        $result = $userVoter->voteOnAttribute('attribute', 'subject', $this->getMockEmptyToken());

        $this->assertFalse($result);
    }

    public function testVoteOnAttributeEditNotAllowed()
    {
        $userVoter = new UserVoter($this->getMockDecisionManager());
        $result = $userVoter->voteOnAttribute('edit', new User(), $this->getMockToken());

        $this->assertFalse($result);
    }

    public function testVoteOnAttributeEditAllowed()
    {
        $userVoter = new UserVoter($this->getMockDecisionManager());
        $token = $this->getMockToken();
        $result = $userVoter->voteOnAttribute('edit', $token->getUser(), $token);

        $this->assertTrue($result);
    }

    public function testVoteOnAttributeViewNotAllowed()
    {
        $userVoter = new UserVoter($this->getMockDecisionManager());
        $result = $userVoter->voteOnAttribute('delete', new User(), $this->getMockToken());

        $this->assertFalse($result);
    }

    public function testVoteOnAttributeViewAllowed()
    {
        $userVoter = new UserVoter($this->getMockDecisionManager());
        $token = $this->getMockToken();
        $result = $userVoter->voteOnAttribute('view', $token->getUser(), $token);

        $this->assertTrue($result);
    }

    public function testVoteOnAttributeLogicException()
    {
        $userVoter = new UserVoter($this->getMockDecisionManager());
        $token = $this->getMockToken();

        $this->expectException(\LogicException::class);

        $userVoter->voteOnAttribute('exception', $token->getUser(), $token);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TokenInterface
     */
    protected function getMockEmptyToken()
    {
        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())
            ->method('getUser')
            ->will($this->returnValue('user'));

        return $token;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TokenInterface
     */
    protected function getMockToken()
    {
        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())
            ->method('getUser')
            ->will($this->returnValue(new User()));

        $token->setUser(new User());

        return $token;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|AccessDecisionManagerInterface
     */
    protected function getMockDecisionManager()
    {
        $decisionManager = $this->createMock(AccessDecisionManagerInterface::class);
        $decisionManager->expects($this->any())
            ->method('decide')
            ->with($this->getMockToken(), ['ROLE_SUPER_ADMIN']);

        return $decisionManager;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|User
     */
    protected function getMockUser()
    {
        $user = $this->createMock(User::class);
        $user->expects($this->once())
            ->method('setRoles')
            ->with(['ROLE_SUPER_ADMIN', 'ROLE_CONFIGURATOR']);

        $user->setRoles(['ROLE_SUPER_ADMIN', 'ROLE_CONFIGURATOR']);

        return $user;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|UserVoter
     */
    protected function getMockUserVoter()
    {
        return $this->getMockBuilder(UserVoter::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([$this->getMockDecisionManager()])
            ->getMock();
    }
}
