<?php

namespace UserBundle\Form\DataTransformer;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Exception\TransformationFailedException;
use UserBundle\Handlers\RoleHandler;

/**
 * @covers \UserBundle\Form\DataTransformer\RoleTransformer
 */
class RoleTransformerTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RoleHandler
     */
    protected $roleHandler;

    protected function setUp()
    {
        $this->roleHandler = $this->createMock(RoleHandler::class);
        $this->roleHandler->expects($this->any())
            ->method('getRolesForForm')
            ->will($this->returnValue(['ROLE_SUPER_ADMIN', 'ROLE_ADMIN']));

        parent::setUp();
    }

    public function testTransform()
    {
        $roleTransformer = new RoleTransformer($this->roleHandler);
        $this->assertEquals('ROLE_ADMIN', $roleTransformer->transform(['ROLE_ADMIN']));
    }

    public function testTransformNullRoles()
    {
        $roleTransformer = new RoleTransformer($this->roleHandler);
        $result = $roleTransformer->transform(null);

        $this->assertEquals('', $result);
    }

    public function testReverseTransform()
    {
        $roleTransformer = new RoleTransformer($this->roleHandler);
        $result = $roleTransformer->reverseTransform('ROLE_ADMIN');

        $this->assertContains('ROLE_ADMIN', $result);
    }

    public function testReverseTransformNoRole()
    {
        $roleTransformer = new RoleTransformer($this->roleHandler);

        $this->expectException(TransformationFailedException::class);
        $roleTransformer->reverseTransform('NO_ROLE');
    }

    public function testReverseTransformNullRole()
    {
        $roleTransformer = new RoleTransformer($this->roleHandler);
        $this->assertInternalType('array', $roleTransformer->reverseTransform(null));
    }
}
