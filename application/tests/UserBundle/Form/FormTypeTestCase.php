<?php

namespace Tests\UserBundle\Form;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Bridge\Doctrine\Test\DoctrineTestHelper;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;

/**
 * Test case used to test form types with entity field.
 *
 * @author Nicolas MACHEREY <nicolas.macherey@gmail.com>
 */
abstract class FormTypeTestCase extends TypeTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @bar ManagerRegistry
     */
    protected $emRegistry;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->em = DoctrineTestHelper::createTestEntityManager();
        $this->emRegistry = $this->createRegistryMock($this->em);

        parent::setUp();

        $this->createSchema();

        $this->factory = Forms::createFormFactoryBuilder()
              ->addExtensions($this->getExtensions())
              ->addTypeExtensions($this->getTypeExtensions())
              ->getFormFactory();

        $this->builder = new FormBuilder(null, null, $this->dispatcher, $this->factory);
    }

    /**
     * Create the schema that will be used for testing.
     */
    protected function createSchema()
    {
        $schemaTool = new SchemaTool($this->em);
        $classes = [];

        foreach ($this->getEntities() as $entityClass) {
            $classes[] = $this->em->getClassMetadata($entityClass);
        }

        try {
            $schemaTool->dropSchema($classes);
        } catch (\Exception $e) {
        }

        try {
            $schemaTool->createSchema($classes);
        } catch (\Exception $e) {
        }
    }

    /**
     * @return array
     */
    protected function getTypeExtensions()
    {
        $validator = $this->getMockBuilder('\Symfony\Component\Validator\Validator\ValidatorInterface')->getMock();
        $validator->method('validate')->will($this->returnValue(new ConstraintViolationList()));

        return [
            new FormTypeValidatorExtension(
                $validator
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getExtensions()
    {
        return array_merge(parent::getExtensions(), array(
            new DoctrineOrmExtension($this->emRegistry),
        ));
    }

    /**
     * Return the entities to map with the entity manager.
     *
     * @return array
     */
    protected function getEntities()
    {
        return array();
    }

    /**
     * Create a mock of entity manager registry.
     *
     * @param EntityManager $em
     *
     * @return \Doctrine\Common\Persistence\ManagerRegistry
     */
    protected function createRegistryMock($em)
    {
        $registry = $this->createMock('Doctrine\Bundle\DoctrineBundle\Registry');
        $registry->expects($this->any())
                 ->method('getManagerForClass')
                 ->will($this->returnValue($em));

        $registry->expects($this->any())
                 ->method('getManagerForClass')
                 ->will($this->returnValue($em));

        return $registry;
    }
}
