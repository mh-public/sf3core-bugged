<?php

namespace Tests\UserBundle\Form\Type;

use Symfony\Component\Form\PreloadedExtension;
use Tests\UserBundle\Form\FormTypeTestCase;
use UserBundle\Entity\User;
use UserBundle\Form\Type\UserType;
use UserBundle\Handlers\RoleHandler;
use UserBundle\Service\CustomerManagerInterface;

/**
 * @covers \UserBundle\Form\Type\UserType
 */
class UserTypeTest extends FormTypeTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RoleHandler
     */
    private $roleHandler;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|CustomerManagerInterface
     */
    private $customerManager;

    protected function setUp()
    {
        $this->roleHandler = $this->createMock(RoleHandler::class);
        $this->roleHandler->expects($this->any())
            ->method('getRolesForForm')
            ->will($this->returnValue(['ROLE_SUPER_ADMIN', 'ROLE_ADMIN']));

        $this->customerManager = $this->createMock(CustomerManagerInterface::class);

        parent::setUp();
    }

    protected function getExtensions()
    {
        $userType = new UserType($this->roleHandler, $this->customerManager);

        return array_merge(parent::getExtensions(), [
            new PreloadedExtension([$userType], []),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntities()
    {
        return array_merge(parent::getEntities(), [
            'UserBundle\Entity\Country',
            'UserBundle\Entity\Team',
            'UserBundle\Entity\Customer',
        ]);
    }

    /**
     * @dataProvider getValidTestData
     */
    public function testSubmitValidData($data)
    {
        $user = new User();
        $form = $this->factory->create(UserType::class, $user, ['loggedUser' => $user]);

        $form->submit($data);

        $this->assertTrue($form->isSynchronized());
        $this->assertSame($user, $form->getData());
        $this->assertSame('test', $user->getNickname());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($data) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function getValidTestData()
    {
        return [
            [
                'data' => [
                    'nickname' => 'test',
                    'email' => 'test2',
                    'roles' => User::ROLE_DEFAULT,
                    'country' => 0,
                    'gender' => User::GENDER_MALE,
                    'plainPassword' => [
                        'first' => 'test',
                        'second' => 'test',
                    ],
                    'customer' => 1,
                ],
            ],
            [
                'data' => [
                    'nickname' => 'test',
                    'email' => 'test2',
                    'country' => 0,
                    'gender' => User::GENDER_MALE,
                    'plainPassword' => [
                        'first' => 'test',
                        'second' => 'test',
                    ],
                    'customer' => 1,
                ],
            ],
        ];
    }
}
