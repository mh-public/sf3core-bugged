<?php

namespace Tests\UserBundle\Service;

use Doctrine\ORM\QueryBuilder;
use UserBundle\Entity\User;
use UserBundle\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \UserBundle\Service\UserManager
 */
class UserManagerTest extends WebTestCase
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $repository;

    public function setUp()
    {
        $class = $this->createClient()->getContainer()->getParameter('fos_user.model.user.class');
        $passwordUpdater = $this->createMock('FOS\UserBundle\Util\PasswordUpdaterInterface');
        $canonicalFieldsUpdater = $this->createMock('FOS\UserBundle\Util\CanonicalFieldsUpdater');
        $this->em = $this->createMock('Doctrine\ORM\EntityManager');
        $this->queryBuilder = $this->createMock('Doctrine\ORM\QueryBuilder');
        $this->repository = $this->createMock('UserBundle\Repository\UserRepository');
        $this->em->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo($class))
            ->will($this->returnValue($this->repository));

        $this->userManager = new UserManager($passwordUpdater, $canonicalFieldsUpdater, $this->em, $class);
    }

    public function testUsersByFilter()
    {
        $criteria = ['filters' => ['role' => User::ROLE_DEFAULT, 'customer' => null]];
        $this->repository->expects($this->once())
            ->method('getUsersByFilter')
            ->with($this->equalTo($criteria))
            ->will($this->returnValue($this->queryBuilder));
        $this->userManager->getUsersByFilter($criteria, new User());
    }
}
