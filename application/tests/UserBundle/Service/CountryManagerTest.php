<?php

namespace Tests\UserBundle\Service;

use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;
use UserBundle\Service\CountryManager;

/**
 * @covers \UserBundle\Service\CountryManager
 */
class CountryManagerTest extends TestCase
{
    const COUNTRY_CLASS = 'UserBundle:Country';

    /**
     * @var CountryManager
     */
    private $countryManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repository;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @throws \ReflectionException
     */
    public function setUp()
    {
        $this->entityManager = $this->createMock('Doctrine\ORM\EntityManager');
        $this->repository = $this->createMock('UserBundle\Repository\CountryRepository');
        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo('UserBundle:Country'))
            ->will($this->returnValue($this->repository));
        $this->queryBuilder = $this->createMock(QueryBuilder::class);

        $this->countryManager = new CountryManager($this->entityManager);
    }

    public function testFindOneByName()
    {
        $criteria = ['name' => 'Argentina'];
        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with($this->equalTo($criteria))
            ->will($this->returnValue([]));
        $this->countryManager->findOneByName('Argentina');
    }

    public function testGetCountriesByFilter()
    {
        $criteria = ['name' => 'Argentina'];
        $this->repository->expects($this->once())
            ->method('getCountriesByFilter')
            ->with($this->equalTo($criteria))
            ->will($this->returnValue($this->queryBuilder));
        $this->countryManager->getCountriesByFilter($criteria);
    }
}
