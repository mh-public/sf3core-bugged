<?php

namespace UserBundle\Handlers;

use PHPUnit\Framework\TestCase;

/**
 * @covers \UserBundle\Handlers\RoleHandler
 */
class RoleHandlerTest extends TestCase
{
    public function testGetRolesForForm()
    {
        $container = $this->createMock('Symfony\Component\DependencyInjection\ContainerInterface');
        $container
            ->expects($this->once())
            ->method('getParameter')
            ->with('security.role_hierarchy.roles')
            ->will($this->returnValue($this->getRoles()));

        $checkerInterface = $this->createMock('Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface');
        $checkerInterface
            ->expects($this->any())
            ->method('isGranted')
            ->withAnyParameters();
        $roleHandler = new RoleHandler($container, $checkerInterface);
        $roles = $roleHandler->getRolesForForm();

        $this->assertGreaterThanOrEqual(1, count($roles));
        $this->assertSame($roles, ['Role User' => 'ROLE_USER']);
    }

    /**
     * @return array
     */
    private function getRoles()
    {
        return [
            'ROLE_USER' => 'ROLE_USER',
            'ROLE_CONFIGURATOR' => 'ROLE_USER',
            'ROLE_SUPER_ADMIN' => 'ROLE_CONFIGURATOR',
        ];
    }
}
