<?php

namespace UserBundle\EventListener;

use CoreBundle\EventListener\SecurityListener;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

class SecurityListenerTest extends WebTestCase
{
    /**
     * @var SecurityListener
     */
    protected $securityListener;

    /**
     * @throws \ReflectionException
     */
    public function setUp()
    {
        $this->securityListener = $this->getMockBuilder(SecurityListener::class)
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setConstructorArgs([
                $this->getMockUrlGenerator(),
                $this->getMockSession(),
                $this->getMockTwigEngine(),
                $this->getMockTokenStorage(),
                $this->getCsrfTokenManager(),
            ])
            ->getMock();
    }

    public function testStub()
    {
        $this->assertTrue(true);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|UrlGeneratorInterface
     *
     * @throws \ReflectionException
     */
    protected function getMockUrlGenerator()
    {
        return $this->createMock(UrlGenerator::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Session
     *
     * @throws \ReflectionException
     */
    protected function getMockSession()
    {
        return $this->createMock(Session::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TwigEngine
     *
     * @throws \ReflectionException
     */
    protected function getMockTwigEngine()
    {
        return $this->createMock(TwigEngine::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|TokenStorage
     *
     * @throws \ReflectionException
     */
    protected function getMockTokenStorage()
    {
        return $this->createMock(TokenStorage::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|CsrfTokenManager
     *
     * @throws \ReflectionException
     */
    protected function getCsrfTokenManager()
    {
        return $this->createMock(CsrfTokenManager::class);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|Request
     *
     * @throws \ReflectionException
     */
    protected function getMockRequest()
    {
        return $this->createMock(Request::class);
    }
}
