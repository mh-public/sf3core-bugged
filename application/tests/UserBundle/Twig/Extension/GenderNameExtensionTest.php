<?php

namespace Tests\UserBundle\Twig\Extension;

use Symfony\Bundle\TwigBundle\Tests\TestCase;
use UserBundle\Entity\User;
use UserBundle\Twig\Extension\GenderNameExtension;

class GenderNameExtensionTest extends TestCase
{
    public function testGenderMale()
    {
        $genderNameExtension = new GenderNameExtension();
        $name = $genderNameExtension->gender(User::GENDER_MALE);

        $this->assertEquals('male', $name);
    }

    public function testGenderFemale()
    {
        $genderNameExtension = new GenderNameExtension();
        $name = $genderNameExtension->gender(User::GENDER_FEMALE);

        $this->assertEquals('female', $name);
    }

    public function testGenderOther()
    {
        $genderNameExtension = new GenderNameExtension();
        $name = $genderNameExtension->gender(User::GENDER_OTHER);

        $this->assertEquals('other', $name);
    }

    public function testGetFilters()
    {
        $genderNameExtension = new GenderNameExtension();
        $filters = $genderNameExtension->getFilters();
        /** @var \Twig_SimpleFilter $filter */
        $filter = reset($filters);

        $this->assertEquals('genderName', $filter->getName());
        $this->assertEquals('gender', $filter->getCallable()[1]);
    }
}
