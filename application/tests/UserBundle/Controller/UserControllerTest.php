<?php

namespace Tests\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\DomCrawler\Field\ChoiceFormField;
use Symfony\Component\DomCrawler\Field\FormField;
use Symfony\Component\DomCrawler\Form;

/**
 * @covers \UserBundle\Controller\UserController
 * @covers \CoreBundle\EventListener\Entity\UserstampableSubscriber
 * @covers \CoreBundle\EventListener\Entity\TimestampableSubscriber
 */
class UserControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $superAdminClient = null;

    /**
     * @var Client
     */
    private $userClient = null;

    public function setUp()
    {
        $this->superAdminClient = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@admin.pl',
            'PHP_AUTH_PW' => 'admin',
        ]);

        $this->userClient = static::createClient([], [
            'PHP_AUTH_USER' => 'default@default.pl',
            'PHP_AUTH_PW' => 'default',
        ]);
    }

    public function testAccessDenied()
    {
        $crawler = $this->userClient->request('GET', '/admin/users');

        $this->assertContains('Access denied', $crawler->text());
        $this->assertEquals(403, $this->userClient->getResponse()->getStatusCode());
    }

    public function testUserAction()
    {
        $crawler = $this->superAdminClient->request('GET', '/admin/users');
        $this->assertEquals(
            'Users',
            $crawler->filter('.box-title')->text()
        );
        $this->assertGreaterThanOrEqual(
            2,
            $crawler->filter('.box-body > table > tbody tr')->count()
        );
    }

    public function testShowAction()
    {
        $crawler = $this->superAdminClient->request('GET', '/admin/user/2/detail');
        $this->assertEquals(
            'User #2',
            $crawler->filter('.box-title')->text()
        );
    }

    public function testNewUserAction()
    {
        $crawler = $this->superAdminClient->request('GET', '/admin/users');
        $newLink = $crawler->filter('.box-header > a.btn-new')->link();

        $crawler = $this->superAdminClient->click($newLink);
        $form = $crawler->selectButton('Save')->form();

        $formData = $this->getNewData($form);

        $this->superAdminClient->submit($form, $formData);
        $this->assertTrue($this->superAdminClient->getResponse()->isRedirect());
        $crawler = $this->superAdminClient->followRedirect();

        $this->assertContains('Users', $crawler->text());
        $this->assertGreaterThanOrEqual(2, $crawler->filter('.box-body > table > tbody tr')->count());
    }

    public function testEditUserAction()
    {
        $crawler = $this->superAdminClient->request('GET', '/admin/users');
        $editLink = $crawler->filter('.box-body > table > tbody > tr')->last()->filter('.edit-btn')->link();

        $crawler = $this->superAdminClient->click($editLink);
        $form = $crawler->selectButton('Save')->form();

        $formData = $this->getEditData();

        $this->superAdminClient->submit($form, $formData);
        $this->assertTrue($this->superAdminClient->getResponse()->isRedirect());
        $crawler = $this->superAdminClient->followRedirect();

        $this->assertContains('Users', $crawler->text());
        $this->assertGreaterThanOrEqual(2, $crawler->filter('.box-body > table > tbody tr')->count());
        $this->assertContains('test5test', $crawler->text());
        $this->assertContains('test5@test.pl', $crawler->text());
    }

    public function testDeleteUserAction()
    {
        $crawler = $this->superAdminClient->request('GET', '/admin/users');
        $rows = $crawler->filter('.box-body > table > tbody > tr');
        $deleteLink = $rows->last()->filter('.delete-btn')->link();
        $this->superAdminClient->click($deleteLink);

        $this->assertTrue($this->superAdminClient->getResponse()->isRedirect());
    }

    /**
     * @param FormField|ChoiceFormField $formTag
     *
     * @return string
     */
    protected function getOptionValue($formTag)
    {
        $formVal = $formTag->availableOptionValues();
        array_shift($formVal);

        return $formVal[array_rand($formVal)];
    }

    /**
     * @param Form $form
     *
     * @return array
     */
    protected function getNewData($form)
    {
        return [
            'user_bundle_user_type[nickname]' => 'test2',
            'user_bundle_user_type[email]' => 'test@test.pl',
            'user_bundle_user_type[plainPassword][first]' => 'Admin1234',
            'user_bundle_user_type[plainPassword][second]' => 'Admin1234',
            'user_bundle_user_type[enabled]' => true,
            'user_bundle_user_type[gender]' => $this->getOptionValue($form['user_bundle_user_type[gender]']),
            'user_bundle_user_type[country]' => $this->getOptionValue($form['user_bundle_user_type[country]']),
            'user_bundle_user_type[birthday]' => '1988-01-01',
        ];
    }

    /**
     * @return array
     */
    protected function getEditData()
    {
        return [
            'user_bundle_user_type[nickname]' => 'test5test',
            'user_bundle_user_type[email]' => 'test5@test.pl',
        ];
    }
}
