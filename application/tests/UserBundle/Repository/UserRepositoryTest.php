<?php

namespace Tests\UserBundle\Repository;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

/**
 * @covers \UserBundle\Repository\UserRepository
 */
class UserRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @param array $params
     * @param int   $expectedCount
     *
     * @dataProvider paramProvider
     */
    public function testSearchByUsername($params, $expectedCount)
    {
        $users = $this->getRepository()
            ->getUsersByFilterResult($params);

        $this->assertCount($expectedCount, $users);
    }

    /**
     * @return array
     */
    public function paramProvider()
    {
        return [
            [
                ['filters' => ['search' => 'admin'], 'order_by' => ['username' => 'ASC']], 1,
            ],
            [
                ['filters' => ['country' => [1, 2]], 'order_by' => ['username' => 'ASC']], 2,
            ],
            [
                ['filters' => ['role' => User::ROLE_SUPER_ADMIN], 'order_by' => ['username' => 'ASC']], 1,
            ],
            [
                ['filters' => ['search' => 'test1'], 'order_by' => ['username' => 'ASC']], 0,
            ],
            [
                ['filters' => ['gender' => User::GENDER_DEFAULT], 'order_by' => ['username' => 'ASC']], 1,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }

    /**
     * @return UserRepository
     */
    private function getRepository()
    {
        return $this->em->getRepository('UserBundle:User');
    }
}
