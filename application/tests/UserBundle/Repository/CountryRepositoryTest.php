<?php

namespace Tests\UserBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UserBundle\Repository\CountryRepository;

/**
 * @covers \UserBundle\Repository\CountryRepository
 */
class CountryRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @param array $params
     * @param int   $expectedCount
     *
     * @dataProvider paramProvider
     */
    public function testSearchByUsername($params, $expectedCount)
    {
        $countries = $this->getRepository()
            ->getCountriesByFilter($params);

        $this->assertInstanceOf(QueryBuilder::class, $countries);
        $this->assertCount($expectedCount, $countries->getQuery()->getArrayResult());
    }

    /**
     * @return array
     */
    public function paramProvider()
    {
        return [
            [['filters' => ['name' => 'TestName'], 'order_by' => ['name' => Criteria::ASC]], 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    /**
     * @return CountryRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository('UserBundle:Country');
    }
}
