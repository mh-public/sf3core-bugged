<?php

namespace Tests\UserBundle\Entity;

use UserBundle\Entity\Country;
use UserBundle\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest.
 *
 * @covers \UserBundle\Entity\User
 */
class UserTest extends TestCase
{
    public function testBirthday()
    {
        $date = new \DateTime();
        $user = $this->getUser();
        $this->assertNull($user->getBirthday());

        $user->setBirthday($date);
        $this->assertSame($date, $user->getBirthday());
    }

    public function testGender()
    {
        $user = $this->getUser();
        $this->assertEquals(0, $user->getGender());

        $user->setGender(User::GENDER_FEMALE);
        $this->assertEquals(User::GENDER_FEMALE, $user->getGender());
    }

    public function testSignUpDate()
    {
        $date = new \DateTime();
        $user = $this->getUser();
        $this->assertNull($user->getSignUpDate());

        $user->setSignUpDate($date);
        $this->assertSame($date, $user->getSignUpDate());
    }

    public function testCountry()
    {
        $country = new Country();
        $user = $this->getUser();
        $this->assertNull($user->getCountry());

        $user->setCountry($country);
        $this->assertSame($country, $user->getCountry());
    }

    public function testPassword()
    {
        $user = $this->getUser();
        $user->setUsername('Test1');
        $user->setPlainPassword('Test1');

        $this->assertFalse($user->isPasswordLegal());

        $user->setPlainPassword('Test2');
        $this->assertTrue($user->isPasswordLegal());

        $user->setPlainPassword(null);
        $this->assertTrue($user->isPasswordLegal());
    }

    public function testGetGenderArray()
    {
        $user = $this->getUser();

        $expectedGenderArray = [
            User::GENDER_DEFAULT,
            User::GENDER_MALE,
            User::GENDER_FEMALE,
            User::GENDER_OTHER,
        ];
        $genderArray = $user->getGendersArray();
        $arrayDiff = array_diff($expectedGenderArray, $genderArray);

        $this->assertInternalType('array', $genderArray);
        $this->assertEquals(0, reset($arrayDiff));
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return $this->getMockForAbstractClass(User::class);
    }
}
