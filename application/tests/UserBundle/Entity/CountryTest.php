<?php

namespace Tests\UserBundle\Entity;

use PHPUnit\Framework\TestCase;
use UserBundle\Entity\Country;
use UserBundle\Entity\User;

/**
 * Class CountryTest.
 *
 * @covers \UserBundle\Entity\Country
 */
class CountryTest extends TestCase
{
    public function testName()
    {
        $country = $this->getCountry();
        $this->assertNull($country->getName());

        $country->setName('Test1');
        $this->assertSame('Test1', $country->getName());
    }

    public function testShortName()
    {
        $country = $this->getCountry();
        $this->assertNull($country->getShortName());

        $country->setShortName('Test1');
        $this->assertSame('Test1', $country->getShortName());
    }

    public function testUser()
    {
        $country = $this->getCountry();
        $user = new User();
        $this->assertEmpty($country->getUsers());

        $country->addUser($user);
        $this->assertContains($user, $country->getUsers());

        $country->removeUser($user);
        $this->assertEmpty($country->getUsers());
    }

    /**
     * @return Country
     */
    protected function getCountry()
    {
        return $this->getMockForAbstractClass(Country::class);
    }
}
