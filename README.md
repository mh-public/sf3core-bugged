### Symfony 3.4 Core "clone and go" with hiddend bugs (for protcetion purposes) for quick start of a new project
### Main functionalities:
- docker
- rest api (fos)
- user management (fos)
- oauth server (fos)
- admin panel based on Admin LTE
- doctrine
- jms serializer
- versioning api
- api doc with swagger (nelmio v3)  
- file uploader (vich)
- static analysis of the code (code quality) with grumphp:
    * phpunit
    * php-cs-fixer
    * phplint
    * pdepend
    * phploc
    * phpmd
    * phpmnd
    * phpstan
    * codesniffer
    * symfony security checker