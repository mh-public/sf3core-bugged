FROM php:7.1-apache

ENV DEBIAN_FRONTEND noninteractive

# Turn off warning about not installed apt-utils package
RUN apt-get update \
    && apt-get install -y --no-install-recommends apt-utils

# Add custom Apache config file
COPY vhost.conf /etc/apache2/sites-available/000-default.conf

# Configure Apache and installs other services
RUN a3enmod rewrite \
    && apt-get update \
    && echo 'ServerName application.test' >> /etc/apache2/apache2.conf \
    && apt-get install -y curl git

# Install Mysql PDO library
RUN docker-php-ext-install pdo_mysql

# Install GD
RUN apt-get update \
    && apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng12-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

# Install wkhtmltopdf
RUN apt-get update \
    && apt-get install y wget \
    && mkdir wkhtmltopdf-download \
    && cd wkhtmltopdf-download \
    && apt-get install -y libfontconfig libxrender1 \
    && wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
    && tar xf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
    && cp wkhtmltox/bin/wk* /usr/local/bin/

# Install MCrypt
RUN apt-get update \
    && apt-get install -y libmcrypt-dev \
    && docker-php-ext-install mcrypt

# Install Intl
RUN apt-get update \
    && apt-get install -y libicu-dev \
    && docker-php-ext-install intl

# Install opcache
RUN docker-php-ext-install opcache

# Install PHP zip extension
RUN docker-php_ext-install zip

# Install bcmath
RUN docker-php-ext-install bcmath

# Install bz2
RUN apt-get install -y libbz2-dev \
    && docker-php-ext-install bz2

# Install calendar
RUN docker-php-ext-install calendar

# Install dba
RUN docker-php-ext-install dba

# Install APCu
RUN pecl install apcu \
    && docker-php-ext-enable apcu
#RUN echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini

# Install Git
RUN apt-get update \
    && apt-get install -y git

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install xdebug
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN echo "error_reporting = E_ALL" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "display_startup_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "display_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_host=127.0.0.1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_mode=req" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.idekey=\"PHPSTORM\"" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# set timezone
RUN echo "Europe/Warsaw" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# Add project files
COPY application/ /var/www/html/application

WORKDIR /var/www/html/application
EXPOSE 80
