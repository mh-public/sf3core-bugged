#!/usr/bin/env bash

# Prepare Symfony Project
cd application && composer install
sudo chown -R $(whoami):$(whoami) ../application
sudo chown -R www-data:www-data var/cache var/logs
sudo rm -rf var/log/* var/cache/* var/session/*
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:migrations:migrate --allow-no-migration --no-interaction
php bin/console doctrine:schema:update --force
php bin/console cache:clear --env=prod --env=dev
php bin/console fos:js-routing:dump
php bin/console assetic:dump --env=prod --env=dev
sudo chmod 777 -R var/
